package org.atesh.bulldozer;

import com.google.common.base.Stopwatch;
import org.apache.commons.cli.*;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.compress.GzipCodec;
import org.apache.spark.Accumulator;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.atesh.commons.docs.AteshDoc;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by saeed on 1/15/15.
 */
public class XmlExtractor {
    public static void main(String[] args) {
        String appName = "Generate XML '";
        appName = appName.replace("'", "\\'");

        CommandLineParser parser = new BasicParser();
        Options options = new Options();
        options.addOption("i", "input", true, "the input path.");
        options.addOption("o", "output", true, "the output path.");
        options.addOption("p", true, "Number of partitions.");
        options.addOption("r", false, "Repartition.");
        //options.addOption("meta", false, "Generate MetaDocs too.");

        String input_path = "";
        String output_path = "";
        int partitions = 1000;
        boolean repart = false;

        try {
            CommandLine line = parser.parse(options, args);

            if (line.hasOption("i"))
                input_path = line.getOptionValue("i");
            else {
                System.out.println("Input must be given!");
                System.exit(-1);
            }

            if (line.hasOption("o")) {
                output_path = line.getOptionValue("o");
            } else {
                System.out.println("Output must be given!");
                System.exit(-1);
            }

            if (line.hasOption("p"))
                partitions = Integer.parseInt(line.getOptionValue("p"));

            if (line.hasOption("r"))
                repart = true;

//            if (line.hasOption("meta"))
//                meta = true;


            SparkConf conf = new SparkConf().setAppName(appName);
            conf.set("spark.executor.memory", "4g");
            conf.set("spark.serializer", "org.apache.spark.serializer.KryoSerializer");
            conf.set("spark.rdd.compress", "true");
            conf.set("spark.hadoop.mapred.output.compress", "true");
            conf.set("spark.hadoop.mapred.output.compression.codec", "true");
            conf.set("spark.hadoop.mapred.output.compression.codec", "org.apache.hadoop.io.compress.GzipCodec");
            conf.set("spark.hadoop.mapred.output.compression.type", "BLOCK");

            JavaSparkContext sc = new JavaSparkContext(conf);
            Stopwatch stopwatch = Stopwatch.createStarted();

            Accumulator<Integer> docCount = sc.accumulator(0);
            Accumulator<Integer> expCount = sc.accumulator(0);
            Accumulator<Integer> filteredCount = sc.accumulator(0);

            JavaPairRDD<LongWritable, AteshDoc> rows = sc.sequenceFile(input_path, LongWritable.class, AteshDoc.class);

            rows = rows.coalesce(partitions);
            rows = rows.filter(t -> {
                boolean isGood = t._2.isGoodForXml();
                if (!isGood)
                    filteredCount.add(1);
                return isGood;
            });

            JavaRDD<String> result = rows.flatMap(t -> {
                docCount.add(1);
                List<String> xml = new ArrayList<String>(1);
                try {
                    xml.add(t._2.toSphinxXml());
                } catch (MalformedURLException exp) {
                    expCount.add(1);
                }
                return xml;
            });

            if (repart)
                result = result.repartition(partitions);

            result.saveAsTextFile(output_path, GzipCodec.class);

            System.out.printf("Docs: %,d \n", docCount.value() + filteredCount.value() + expCount.value());
            System.out.printf("Kept: %,d \n", docCount.value());
            System.out.printf("Filtered: %,d \n", filteredCount.value());
            System.out.printf("Exp: %,d \n", expCount.value());
            System.out.println("Time: " + stopwatch);

            return;


        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
