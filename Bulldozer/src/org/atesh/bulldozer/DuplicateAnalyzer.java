package org.atesh.bulldozer;

import com.google.common.base.Stopwatch;
import org.apache.spark.Accumulator;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.storage.StorageLevel;
import scala.Tuple2;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by saeed on 1/14/15.
 */
public class DuplicateAnalyzer {
    public static void main(String[] args) {
        String appName = "Duplicate Analyzer";
        SparkConf conf = new SparkConf().setAppName(appName);
        conf.set("spark.executor.memory", "16g");
        conf.set("spark.serializer", "org.apache.spark.serializer.KryoSerializer");
        JavaSparkContext sc = new JavaSparkContext(conf);

        Stopwatch stopwatch = Stopwatch.createStarted();

        Accumulator<Integer> docCount = sc.accumulator(0);
        JavaRDD<PageInfo> rows = sc.objectFile(args[0]);

        final int CONTENT = 1;
        final int RAW = 2;
        final int WHITE = 3;
        final int DIGIT = 4;
        final int LETTER = 5;

        Accumulator<Integer> counter = sc.accumulator(0);
        JavaPairRDD<Integer, String> data = rows.coalesce(1000).flatMapToPair(t ->
                {
                    counter.add(1);
                    List<Tuple2<Integer, String>> list = new ArrayList<Tuple2<Integer, String>>();
                    if (t == null)
                        return list;
                    list.add(new Tuple2(CONTENT, t.host + t.contentHash));
                    list.add(new Tuple2(RAW, t.host + t.rawHash));
                    list.add(new Tuple2(WHITE, t.host + t.whiteHash));
                    list.add(new Tuple2(DIGIT, t.host + t.digitHash));
                    list.add(new Tuple2(LETTER, t.host + t.letterHash));

                    return list;
                }
        );
        data.persist(StorageLevel.MEMORY_AND_DISK());

        //long row_count = rows.count();
        long content_hash_count = data.filter(t -> t._1 == CONTENT).map(t -> t._2).distinct().count();
        long raw_hash_count = data.filter(t -> t._1 == RAW).map(t -> t._2).distinct().count();
        long white_hash_count = data.filter(t -> t._1 == WHITE).map(t -> t._2).distinct().count();
        long digit_hash_count = data.filter(t -> t._1 == DIGIT).map(t -> t._2).distinct().count();
        long letter_hash_count = data.filter(t -> t._1 == LETTER).map(t -> t._2).distinct().count();

        System.out.printf("Number of docs: %,d %n", counter.value());
        System.out.printf("content: %,d %n", content_hash_count);
        System.out.printf("raw: %,d %n", raw_hash_count);
        System.out.printf("white: %,d %n", white_hash_count);
        System.out.printf("digit: %,d %n", digit_hash_count);
        System.out.printf("letter: %,d %n", letter_hash_count);
        System.out.println("Done! " + stopwatch.toString());
    }
}
