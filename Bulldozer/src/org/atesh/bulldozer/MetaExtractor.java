package org.atesh.bulldozer;

import com.google.common.base.Stopwatch;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.spark.Accumulator;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.atesh.commons.docs.AteshDoc;

/**
 * Created by saeed on 1/15/15.
 */
public class MetaExtractor {
    public static void main(String[] args) {
        String appName = "Test";
        SparkConf conf = new SparkConf().setAppName(appName);
        conf.set("spark.executor.memory", "8g");
        conf.set("spark.serializer", "org.apache.spark.serializer.KryoSerializer");
        conf.set("spark.rdd.compress", "true");
        conf.set("spark.hadoop.mapred.output.compress", "true");
        conf.set("spark.hadoop.mapred.output.compression.codec", "true");
        conf.set("spark.hadoop.mapred.output.compression.codec", "org.apache.hadoop.io.compress.GzipCodec");
        conf.set("spark.hadoop.mapred.output.compression.type", "BLOCK");
        conf.set("spark.hadoop.dfs.replication", "2");

        JavaSparkContext sc = new JavaSparkContext(conf);
        Stopwatch stopwatch = Stopwatch.createStarted();


        Accumulator<Integer> docCount = sc.accumulator(0);

        sc.sequenceFile("hdfs://psh-master:9000/682dd/full/", LongWritable.class, AteshDoc.class).coalesce(1000).mapToPair(t -> {
            t._2.stripContent();
            return t;
        }).saveAsNewAPIHadoopFile("hdfs://psh-master:9000/682dd/light/", LongWritable.class, AteshDoc.class, SequenceFileOutputFormat.class);

        System.out.printf("Docs: %,d \n", docCount.value());
        System.out.println("Time: " + stopwatch);


        return;

    }
}
