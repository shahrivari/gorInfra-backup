package org.atesh.bulldozer;

import com.google.common.base.Stopwatch;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableInputFormat;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;

/**
 * Created by saeed on 1/15/15.
 */
public class HBaseTest {
    public static void main(String[] args) {
        String appName = "Test";
        SparkConf conf = new SparkConf().setAppName(appName);
        conf.set("spark.executor.memory", "24g");
        conf.set("spark.serializer", "org.apache.spark.serializer.KryoSerializer");
        conf.set("spark.rdd.compress", "true");
        conf.set("spark.hadoop.mapred.output.compress", "true");
        conf.set("spark.hadoop.mapred.output.compression.codec", "true");
        conf.set("spark.hadoop.mapred.output.compression.codec", "org.apache.hadoop.io.compress.GzipCodec");
        conf.set("spark.hadoop.mapred.output.compression.type", "BLOCK");
        conf.set("spark.hadoop.dfs.replication", "2");

        JavaSparkContext sc = new JavaSparkContext(conf);
        Configuration hconf = HBaseConfiguration.create();
        hconf.set("hbase.zookeeper.quorum", "rnk-1,rnk-2,rnk-3");
        hconf.set("hbase.zookeeper.property.clientPort", "2181");
        hconf.set(TableInputFormat.INPUT_TABLE, "t");

        JavaPairRDD<ImmutableBytesWritable, Result> hBaseRDD = sc.newAPIHadoopRDD(hconf, TableInputFormat.class,
                ImmutableBytesWritable.class,
                Result.class);

        long count = hBaseRDD.count();

//        Tuple2<ImmutableBytesWritable, Result> xxx = hBaseRDD.first();
//
//        NavigableMap<byte[], byte[]> kv = xxx._2.getFamilyMap(Bytes.toBytes("A"));
//        HashMap<Long, String> mmm = new HashMap<Long, String>();
//        for(Map.Entry<byte[], byte[]> pashm:kv.entrySet())
//            mmm.put(Bytes.toLong(pashm.getKey()),Bytes.toString(pashm.getValue()));


        Stopwatch stopwatch = Stopwatch.createStarted();

        System.out.println("Count: " + count);
        System.out.println("Time: " + stopwatch);


        return;

    }
}
