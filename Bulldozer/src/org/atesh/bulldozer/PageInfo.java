package org.atesh.bulldozer;

import com.google.common.hash.Hashing;
import org.apache.hadoop.io.Writable;
import org.atesh.commons.docs.SeyyedDocV1;
import org.atesh.commons.url.URLUtils;
import org.atesh.commons.util.WordRepeatSerializer;
import org.msgpack.annotation.Message;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by saeed on 1/13/15.
 */

@Message
public class PageInfo implements Writable, Serializable {
    public long id = 0l;
    public String url = "";
    public String title = "";
    public String headers = "";
    public Map<String, Integer> anchors = new HashMap<String, Integer>();

    public String domain = "";
    public String host = "";
    public double pageRank = 0.0f;
    public double pageAuthority = 0.0f;

    public int contentLength = 0;
    public String contentHash = "";
    public int mainContentLength = 0;
    public String mainContentHash = "";

    public String rawHash = "";
    public String whiteHash = "";
    public String digitHash = "";
    public String letterHash = "";

    public PageInfo() {
    }

    private static String sha256(String s) {
        return Hashing.sha256().hashString(s, Charset.defaultCharset()).toString();
    }

    public static PageInfo fromSeyyedDocV1(SeyyedDocV1 sdoc) throws IOException {
        PageInfo result = new PageInfo();
        result.id = sdoc.id;
        result.url = sdoc.originalURL;
        result.title = sdoc.title.substring(0, Math.min(sdoc.title.length(), 128));
        result.headers = sdoc.headings.substring(0, Math.min(sdoc.headings.length(), 128));
        result.anchors = WordRepeatSerializer.deserialize(sdoc.machineKeywords);

        result.domain = URLUtils.getDomain(result.url);
        result.host = URLUtils.getHostName(result.url);
        result.pageRank = sdoc.pageRank;
        result.pageAuthority = sdoc.pq_pageAuthority;

        result.contentLength = sdoc.content.length();
        result.mainContentLength = sdoc.mainContent.length();


        result.contentHash = sha256(sdoc.content);
        result.mainContentHash = sha256(sdoc.mainContent);

        String text = sdoc.title + sdoc.headings + sdoc.content + sdoc.machineKeywords;
        result.rawHash = sha256(text);

        text = text.replaceAll("\\s+", "");
        result.whiteHash = sha256(text);

        text = text.replaceAll("\\d+", "");
        result.digitHash = sha256(text);

        text = text.replaceAll("\\P{L}+", "");
        result.letterHash = sha256(text);

        return result;
    }

    @Override
    public void write(DataOutput dataOutput) throws IOException {
        dataOutput.writeLong(id);
        dataOutput.writeUTF(url);
        dataOutput.writeUTF(title);
        dataOutput.writeUTF(headers);

        dataOutput.writeUTF(domain);
        dataOutput.writeUTF(host);
        dataOutput.writeDouble(pageRank);
        dataOutput.writeDouble(pageAuthority);

        dataOutput.writeInt(contentLength);
        dataOutput.writeInt(mainContentLength);

        dataOutput.writeUTF(contentHash);
        dataOutput.writeUTF(mainContentHash);
        dataOutput.writeUTF(rawHash);
        dataOutput.writeUTF(whiteHash);
        dataOutput.writeUTF(digitHash);
        dataOutput.writeUTF(letterHash);

        dataOutput.writeInt(anchors.size());
        for (Map.Entry<String, Integer> ent : anchors.entrySet()) {
            dataOutput.writeUTF(ent.getKey());
            dataOutput.writeInt(ent.getValue());
        }
    }

    @Override
    public void readFields(DataInput dataInput) throws IOException {
        url = dataInput.readUTF();
        title = dataInput.readUTF();
        headers = dataInput.readUTF();
        domain = dataInput.readUTF();
        host = dataInput.readUTF();
        pageRank = dataInput.readDouble();
        pageAuthority = dataInput.readDouble();
        contentLength = dataInput.readInt();
        mainContentLength = dataInput.readInt();
        contentHash = dataInput.readUTF();
        mainContentHash = dataInput.readUTF();
        rawHash = dataInput.readUTF();
        whiteHash = dataInput.readUTF();
        digitHash = dataInput.readUTF();
        letterHash = dataInput.readUTF();

        int anch_count = dataInput.readInt();
        for (int i = 0; i < anch_count; i++)
            anchors.put(dataInput.readUTF(), dataInput.readInt());
    }


}
