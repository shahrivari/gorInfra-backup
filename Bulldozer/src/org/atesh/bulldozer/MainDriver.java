package org.atesh.bulldozer;

import org.apache.hadoop.util.ProgramDriver;

/**
 * Created by saeed on 1/30/14.
 */
public class MainDriver {
    public static void main(String argv[]) {
        int exitCode = -1;
        ProgramDriver pgd = new ProgramDriver();
        try {

            pgd.driver(argv);
            // Success
            exitCode = 0;
        } catch (Throwable e) {
            e.printStackTrace();
        }
        System.exit(exitCode);
    }
}
