package org.atesh.bulldozer;

import com.google.common.base.Stopwatch;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.spark.Accumulator;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.atesh.commons.docs.AteshDocV2;
import scala.Tuple2;

/**
 * Created by saeed on 1/15/15.
 */
public class AteshDocToJson {
    public static void main(String[] args) {
        String appName = "AteshDoc 2 Json";
        SparkConf conf = new SparkConf().setAppName(appName);
        conf.set("spark.executor.memory", "24g");
        conf.set("spark.serializer", "org.apache.spark.serializer.KryoSerializer");
        conf.set("spark.rdd.compress", "true");
        conf.set("spark.hadoop.mapred.output.compress", "true");
        conf.set("spark.hadoop.mapred.output.compression.codec", "true");
        conf.set("spark.hadoop.mapred.output.compression.codec", "org.apache.hadoop.io.compress.GzipCodec");
        conf.set("spark.hadoop.mapred.output.compression.type", "BLOCK");
        conf.set("spark.hadoop.dfs.replication", "2");

        JavaSparkContext sc = new JavaSparkContext(conf);
        Stopwatch stopwatch = Stopwatch.createStarted();


        Accumulator<Integer> docCount = sc.accumulator(0);
        Accumulator<Integer> expCount = sc.accumulator(0);

        JavaRDD<AteshDocV2> rows = sc.objectFile(args[0]);
        rows.mapToPair(doc -> new Tuple2<LongWritable, Text>(new LongWritable(doc.baghali), new Text(doc.toJson()))).coalesce(1000).saveAsNewAPIHadoopFile(args[1], LongWritable.class, Text.class, SequenceFileOutputFormat.class);

        System.out.printf("Docs: %,d \n", docCount.value());
        System.out.println("Time: " + stopwatch);


        return;

    }
}
