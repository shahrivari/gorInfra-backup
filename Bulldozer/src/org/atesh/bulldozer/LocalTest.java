package org.atesh.bulldozer;

import com.google.common.base.Stopwatch;
import com.google.gson.Gson;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.Text;
import org.apache.spark.Accumulator;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;
import scala.tools.nsc.doc.model.Object;

import java.util.Map;

/**
 * Created by saeed on 1/15/15.
 */
public class LocalTest {
    public static void main(String[] args) {
        String appName = "Local Test";
        SparkConf conf = new SparkConf().setAppName(appName).setMaster("local[4]");
        conf.set("spark.executor.memory", "2g");
        conf.set("spark.serializer", "org.apache.spark.serializer.KryoSerializer");
        conf.set("spark.rdd.compress", "true");
        //conf.set("spark.hadoop.mapred.output.compress", "true");
        //conf.set("spark.hadoop.mapred.output.compression.codec", "org.apache.hadoop.io.compress.GzipCodec");
        //conf.set("spark.hadoop.mapred.output.compression.type", "BLOCK");
        conf.set("spark.hadoop.dfs.replication", "2");

        JavaSparkContext sc = new JavaSparkContext(conf);

        Stopwatch stopwatch = Stopwatch.createStarted();

        JavaPairRDD<Text, BytesWritable> rr = sc.sequenceFile("hdfs://i7-01:9000/vahid/*", Text.class, BytesWritable.class);
        Accumulator<Integer> docCount = sc.accumulator(0);

        String s = rr.first()._1.toString();

        Gson gson = new Gson();
        Map<String, Object> map = gson.fromJson(s, Map.class);


        System.out.printf("Docs: %,d \n", docCount.value());


        System.exit(0);

        return;

    }
}
