package org.atesh.bulldozer;

import com.google.common.base.Stopwatch;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.spark.Accumulator;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.atesh.commons.docs.AteshDoc;
import org.atesh.commons.docs.SeyyedDocV1;
import org.atesh.commons.util.SQLUtils;
import scala.Tuple2;

import java.util.ArrayList;

/**
 * Created by saeed on 2/2/15.
 */
public class Test {
    public static void main(String[] args) {
        String appName = "Test";
        SparkConf conf = new SparkConf().setAppName(appName);
        conf.set("spark.executor.memory", "8g");
        conf.set("spark.serializer", "org.apache.spark.serializer.KryoSerializer");
        conf.set("spark.rdd.compress", "true");
        conf.set("spark.hadoop.mapred.output.compress", "true");
        conf.set("spark.hadoop.mapred.output.compression.codec", "true");
        conf.set("spark.hadoop.mapred.output.compression.codec", "org.apache.hadoop.io.compress.GzipCodec");
        conf.set("spark.hadoop.mapred.output.compression.type", "BLOCK");
        conf.set("spark.hadoop.dfs.replication", "2");

        JavaSparkContext sc = new JavaSparkContext(conf);
        Stopwatch stopwatch = Stopwatch.createStarted();


        String input_path = "hdfs://ilab-1:8020//user/indexer/analysis/categories/medical/*";
        Accumulator<Integer> docCount = sc.accumulator(0);
        Accumulator<Integer> expCount = sc.accumulator(0);

        JavaPairRDD<LongWritable, Text> rows = sc.sequenceFile(input_path, LongWritable.class, Text.class);

        JavaPairRDD<String, AteshDoc> hashed = rows.flatMapToPair(t -> {
                    docCount.add(1);
                    ArrayList<Tuple2<String, AteshDoc>> list = new ArrayList<Tuple2<String, AteshDoc>>();
                    SeyyedDocV1 sdoc = SeyyedDocV1.fromJson(t._2.toString());
                    try {
                        String hash = sdoc.getFigerPrint();
                        list.add(new Tuple2(hash, AteshDoc.fromSeyyedDocV1(sdoc)));
                    } catch (Exception exp) {
                        expCount.add(1);
                    }
                    return list;
                }
        );

        hashed = hashed.repartition(400);

        JavaRDD<AteshDoc> docs = hashed.map(t -> {
            t._2.normalize();
            return t._2;
        });

        JavaPairRDD<AteshDoc, Long> xx = docs.zipWithIndex();
        docs = xx.map(t -> {
            t._1.id = t._2 + 1;
            return t._1;
        });

        JavaRDD<String> sql = docs.map(t -> {
            StringBuilder builder = new StringBuilder();
            builder.append("insert into mdocs values(");
            builder.append(t.id).append(",");
            builder.append("'").append(SQLUtils.EscapeString(t.url)).append("',");
            builder.append("'").append(SQLUtils.EscapeString(t.title)).append("',");
            builder.append("'").append(SQLUtils.EscapeString(t.content)).append("');");
            return builder.toString();
        });

        sql.saveAsTextFile("hdfs://psh-master:9000/mdocs");


//        JavaPairRDD<LongWritable, AteshDoc> rows = sc.sequenceFile(args[0], LongWritable.class, AteshDoc.class);
//        rows = rows.coalesce(5000).repartition(Integer.parseInt(args[2]));
//
//        rows.saveAsNewAPIHadoopFile(args[1] + "/full/", LongWritable.class, AteshDoc.class, SequenceFileOutputFormat.class);
//        sc.sequenceFile(args[1] + "/full/", LongWritable.class, AteshDoc.class).coalesce(Integer.parseInt(args[2])).mapToPair(t -> {
//            docCount.add(1);
//            t._2.stripContent();
//            return t;
//        }).saveAsNewAPIHadoopFile(args[1] + "/meta/", LongWritable.class, AteshDoc.class, SequenceFileOutputFormat.class);

        System.out.printf("Docs: %,d \n", docCount.value());
        System.out.println("Time: " + stopwatch);


        return;

    }

}
