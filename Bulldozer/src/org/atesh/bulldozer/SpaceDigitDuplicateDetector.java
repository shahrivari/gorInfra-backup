package org.atesh.bulldozer;

import com.google.common.base.Stopwatch;
import org.apache.spark.Accumulator;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import scala.Tuple2;

/**
 * Created by saeed on 1/14/15.
 */
public class SpaceDigitDuplicateDetector {
    public static void main(String[] args) {
        String appName = "Duplicate Analyzer";
        SparkConf conf = new SparkConf().setAppName(appName);
        conf.set("spark.executor.memory", "16g");
        conf.set("spark.serializer", "org.apache.spark.serializer.KryoSerializer");
        JavaSparkContext sc = new JavaSparkContext(conf);

        Stopwatch stopwatch = Stopwatch.createStarted();

        Accumulator<Integer> docCount = sc.accumulator(0);
        JavaRDD<PageInfo> rows = sc.objectFile(args[0]);

        JavaPairRDD<String, String> data = rows.coalesce(1000).filter(t -> t != null).mapToPair(t ->
                {
                    docCount.add(1);
                    return new Tuple2(t.digitHash, t.url.replace(' ', '+').replace("\t", "%0B").replace("\n", "%0A"));
                }
        );


        JavaPairRDD<String, String> reduced = data.reduceByKey((a, b) -> a + "<->" + b).filter(t -> t._2.contains("<->"));
        reduced.coalesce(100).saveAsTextFile(args[1]);

        System.out.printf("Number of docs: %,d %n", docCount.value());
        System.out.println("Done! " + stopwatch.toString());
    }
}
