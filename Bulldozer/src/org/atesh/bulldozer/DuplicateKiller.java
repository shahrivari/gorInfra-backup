package org.atesh.bulldozer;

/**
 * Created by saeed on 1/13/15.
 */

import com.google.common.base.Stopwatch;
import org.apache.commons.cli.*;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.spark.Accumulator;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.atesh.commons.docs.AteshDoc;
import org.atesh.commons.docs.SeyyedDocV1;
import scala.Tuple2;

import java.io.IOException;
import java.util.ArrayList;


public class DuplicateKiller {

    public static void main(String[] args) throws IOException {
        CommandLineParser parser = new BasicParser();
        Options options = new Options();
        options.addOption("i", "input", true, "the input path.");
        options.addOption("o", "output", true, "the output path.");
        options.addOption("snappy", true, "Use Snappy codec instead of Gzip.");
        options.addOption("nm", true, "Number of mappers.");
        options.addOption("nr", true, "Number of reducers.");
        String codec = "org.apache.hadoop.io.compress.GzipCodec";
        HelpFormatter formatter = new HelpFormatter();

        String input_path = "";
        String output_path = "";
        int mappers = 5000;
        int reducers = 1000;

        try {
            CommandLine line = parser.parse(options, args);

            if (line.hasOption("i"))
                input_path = line.getOptionValue("i");
            else {
                System.out.println("Input must be given!");
                System.exit(-1);
            }

            if (line.hasOption("o")) {
                output_path = line.getOptionValue("o");
            } else {
                System.out.println("Output must be given!");
                System.exit(-1);
            }

            if (line.hasOption("nr"))
                reducers = Integer.parseInt(line.getOptionValue("nr"));

            if (line.hasOption("nm"))
                mappers = Integer.parseInt(line.getOptionValue("nm"));


            String appName = "Duplicate Killer!!!";
            SparkConf conf = new SparkConf().setAppName(appName);
            conf.set("spark.executor.memory", "4g");
            conf.set("spark.serializer", "org.apache.spark.serializer.KryoSerializer");
            conf.set("spark.rdd.compress", "true");
            conf.set("spark.hadoop.mapred.output.compress", "true");
            conf.set("spark.hadoop.mapred.output.compression.codec", "true");
            conf.set("spark.hadoop.mapred.output.compression.codec", codec);
            conf.set("spark.hadoop.mapred.output.compression.type", "BLOCK");
            JavaSparkContext sc = new JavaSparkContext(conf);

            Stopwatch stopwatch = Stopwatch.createStarted();


            Accumulator<Integer> docCount = sc.accumulator(0);
            Accumulator<Integer> uniqueCount = sc.accumulator(0);
            Accumulator<Integer> expCount = sc.accumulator(0);
            JavaPairRDD<LongWritable, Text> rows = sc.sequenceFile(input_path, LongWritable.class, Text.class);
            rows = rows.coalesce(mappers);

            JavaPairRDD<String, AteshDoc> hashed = rows.flatMapToPair(t -> {
                        docCount.add(1);
                        ArrayList<Tuple2<String, AteshDoc>> list = new ArrayList<Tuple2<String, AteshDoc>>();
                        SeyyedDocV1 sdoc = SeyyedDocV1.fromJson(t._2.toString());
                        try {
                            String hash = sdoc.getFigerPrint();
                            list.add(new Tuple2(hash, AteshDoc.fromSeyyedDocV1(sdoc)));
                        } catch (Exception exp) {
                            expCount.add(1);
                        }
                        return list;
                    }
            );


            JavaPairRDD<String, AteshDoc> reduced = hashed.reduceByKey((a, b) -> {
                if (a.url.length() < b.url.length()) {
                    a.pageRank = Math.max(a.pageRank, b.pageRank);
                    return a;
                } else {
                    b.pageRank = Math.max(a.pageRank, b.pageRank);
                    return b;
                }
            }, reducers);

            JavaPairRDD<LongWritable, AteshDoc> uniques = reduced.map(t -> t._2).zipWithUniqueId().map(t -> {
                t._1.id = t._2 + 1;
                uniqueCount.add(1);
                return t._1;
            }).mapToPair(doc -> new Tuple2<LongWritable, AteshDoc>(new LongWritable(doc.id), doc));

            uniques.saveAsNewAPIHadoopFile(output_path + "/full/", LongWritable.class, AteshDoc.class, SequenceFileOutputFormat.class);

            sc.sequenceFile(output_path + "/full/", LongWritable.class, AteshDoc.class).coalesce(reducers).mapToPair(t -> {
                t._2.stripContent();
                return t;
            }).saveAsNewAPIHadoopFile(output_path + "/light/", LongWritable.class, AteshDoc.class, SequenceFileOutputFormat.class);


            System.out.println("Done! " + stopwatch.toString());
            System.out.printf("#Docs: %,d \n", docCount.value());
            System.out.printf("#Uniques: %,d \n", uniqueCount.value());
            System.out.printf("#Duplicates: %,d \n", docCount.value() - uniqueCount.value());
            System.out.printf("#Exceptions: %,d \n", expCount.value());


        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

}
