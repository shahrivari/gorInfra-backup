package org.atesh.hawk;

import com.google.common.base.Stopwatch;
import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import scala.Tuple2;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Main {

    public static void main(String[] args) {
        String appName="testtttt";
        appName="pashm";
        Character cc='h';
        int xx=(int)cc;
        //String master="spark://sn-1:7077";
        SparkConf conf = new SparkConf().setAppName(appName);
        conf.set("spark.executor.memory","8g");
        conf.set("spark.serializer", "org.apache.spark.serializer.KryoSerializer");
        JavaSparkContext sc = new JavaSparkContext(conf);

        Stopwatch stopwatch= Stopwatch.createStarted();
        JavaPairRDD<LongWritable, Text> rows = sc.sequenceFile(args[0], LongWritable.class, Text.class);
        rows = rows.coalesce(5000);



        JavaPairRDD<Character,Long> pairs = rows.flatMapToPair(t -> {
            Gson gson = new GsonBuilder().create();
            Map<String, String> map = gson.fromJson(t._2.toString(), Map.class);
            String content = map.get("content");
            if (content == null)
                return new ArrayList<Tuple2<Character, Long>>();
            Multiset<Character> chars= HashMultiset.create();
            for(int i=0;i<content.length();i++)
                chars.add(content.charAt(i));
            List<Tuple2<Character,Long>> list=new ArrayList<Tuple2<Character, Long>>();
            for(Character ch:chars.elementSet()){
                //list.add(new Tuple2<Character,Long>(ch,(long)chars.count(ch)));
                list.add(new Tuple2<Character,Long>(ch,1l));
            }
            return list;
        });


        JavaPairRDD<Character, Long> counts = pairs.reduceByKey((a, b) -> a + b,100);
        System.out.printf("MapCount %,d\n",counts.count());

        JavaPairRDD<Long, Character> rev = counts.mapToPair(t -> new Tuple2<Long, Character>(t._2, t._1)).sortByKey(false);
        JavaRDD<String> res = rev.map(t -> String.format("%,d -> [%c] U+%04x", t._1,t._2.isWhitespace(t._2)? ' ': t._2 ,(int)t._2));
        res.coalesce(1).saveAsTextFile(args[1]);


        System.out.println("Done! "+stopwatch.toString());
    }
}