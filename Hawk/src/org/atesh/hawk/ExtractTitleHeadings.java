package org.atesh.hawk;

import com.google.common.base.Stopwatch;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.atesh.commons.docs.SeyyedDocV1;
import scala.Tuple2;

import java.io.IOException;

public class ExtractTitleHeadings {

    public static void main(String[] args) throws IOException {

        String appName = "TitHead";
        SparkConf conf = new SparkConf().setAppName(appName);
        conf.set("spark.executor.memory", "24g");
        conf.set("spark.serializer", "org.apache.spark.serializer.KryoSerializer");
        JavaSparkContext sc = new JavaSparkContext(conf);

        Stopwatch stopwatch = Stopwatch.createStarted();
        JavaPairRDD<LongWritable, Text> rows = sc.sequenceFile(args[0], LongWritable.class, Text.class);
        rows = rows.coalesce(5000);


        JavaPairRDD<String, String> pairs = rows.mapToPair(t -> {
            SeyyedDocV1 sdoc = SeyyedDocV1.fromJson(t._2.toString());
            String content = sdoc.title + " " + sdoc.headings;
            return new Tuple2<String, String>(sdoc.url, content);
        });

        pairs.coalesce(5000).saveAsObjectFile(args[1]);
        System.out.println("Done! " + stopwatch.toString());
    }
}