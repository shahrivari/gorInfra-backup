package org.atesh.hawk;

import com.google.common.base.Stopwatch;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.storage.StorageLevel;
import org.atesh.commons.docs.SeyyedDocV1;
import org.atesh.commons.text.PersoLatinCharactarFilter;
import org.atesh.commons.text.SimpleTokenizer;
import org.atesh.commons.text.SoftNormalizer;
import scala.Tuple2;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class DictionaryBuilder {

    public static void main(String[] args) throws IOException {

        String appName = "Dictionary Builder";
        SparkConf conf = new SparkConf().setAppName(appName);
        conf.set("spark.executor.memory", "24g");
        conf.set("spark.serializer", "org.apache.spark.serializer.KryoSerializer");
        JavaSparkContext sc = new JavaSparkContext(conf);

        Stopwatch stopwatch = Stopwatch.createStarted();
        JavaPairRDD<LongWritable, Text> rows = sc.sequenceFile(args[0], LongWritable.class, Text.class);
        rows = rows.coalesce(5000);


        JavaPairRDD<String, Long> pairs = rows.flatMapToPair(t -> {
            SeyyedDocV1 sdoc = SeyyedDocV1.fromJson(t._2.toString());
            String content = sdoc.title + " " + sdoc.headings;
            if (content == null)
                return new ArrayList<Tuple2<String, Long>>();

            content = new SoftNormalizer().normalize(content);
            content = new PersoLatinCharactarFilter().filterToSpace(content);
            List<String> tokens = new SimpleTokenizer().tokenize(content);

            HashSet<String> ngrams = new HashSet<String>();
            ngrams.addAll(tokens);
            ArrayList<Tuple2<String, Long>> list = new ArrayList<Tuple2<String, Long>>();

            for (String str : ngrams) {
                list.add(new Tuple2<String, Long>(str, 1l));
            }
            return list;
        });

        JavaPairRDD<String, Long> reduced = pairs.reduceByKey((a, b) -> a + b, 500);
        reduced.persist(StorageLevel.DISK_ONLY());
        JavaPairRDD<Long, String> one_grams = reduced.filter(t -> t._2 > 9).mapToPair(t -> new Tuple2<Long, String>(t._2, t._1)).sortByKey(false, 100);
        one_grams.saveAsTextFile(args[1] + "//words");

        JavaPairRDD<Long, Long> counts = reduced.mapToPair(t -> new Tuple2<Long, Long>(t._2, 1l));
        counts.reduceByKey((a, b) -> a + b).sortByKey(false, 100).coalesce(10).saveAsTextFile(args[1] + "//freqs");


        long wordcount = reduced.count();
        System.out.println("Done! " + stopwatch.toString());
        System.out.printf("Number of words: %,d %n", wordcount);
    }
}