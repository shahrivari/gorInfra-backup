package org.atesh.hawk;

import com.google.common.base.Stopwatch;
import org.apache.hadoop.io.compress.GzipCodec;
import org.apache.spark.Accumulator;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.atesh.commons.docs.AteshMetaDoc;
import org.atesh.commons.text.StringUtils;

/**
 * Created by saeed on 1/15/15.
 */
public class GenerateMetaXML {
    public static void main(String[] args) {
        String appName = "Generate Meta XML";
        SparkConf conf = new SparkConf().setAppName(appName);
        conf.set("spark.executor.memory", "8g");
        conf.set("spark.serializer", "org.apache.spark.serializer.KryoSerializer");
        conf.set("spark.rdd.compress", "true");
        conf.set("spark.hadoop.mapred.output.compress", "true");
        conf.set("spark.hadoop.mapred.output.compression.codec", "true");
        conf.set("spark.hadoop.mapred.output.compression.codec", "org.apache.hadoop.io.compress.GzipCodec");
        conf.set("spark.hadoop.mapred.output.compression.type", "BLOCK");

        JavaSparkContext sc = new JavaSparkContext(conf);
        Stopwatch stopwatch = Stopwatch.createStarted();

        Accumulator<Integer> docCount = sc.accumulator(0);
        JavaRDD<AteshMetaDoc> rows = sc.objectFile(args[0]);
        rows = rows.coalesce(1000);
        rows = rows.filter(meta -> meta.isGood()).repartition(Integer.parseInt(args[2]));

        JavaRDD<String> result = rows.map(meta -> {
            docCount.add(1);
            meta.refine();
            String sit = meta.title.substring(0, Math.min(meta.title.length(), 64));
            StringBuilder xml = new StringBuilder();
            xml.append("<sphinx:document id=\"").append(meta.id + 1).append("\">\n");
            xml.append("<baghali>").append(meta.baghali).append("</baghali>\n");
            xml.append("<url>").append(StringUtils.EscapeXML(meta.url)).append("</url>\n");
            xml.append("<sit>").append(StringUtils.EscapeXML(sit)).append("</sit>\n");
            xml.append("<tit>").append(StringUtils.EscapeXML(meta.title)).append("</tit>\n");
            xml.append("<hdr>").append(StringUtils.EscapeXML(meta.headings)).append("</hdr>\n");
            xml.append("<anc>").append(StringUtils.EscapeXML(meta.anchors)).append("</anc>\n");
            xml.append("<dom>").append(StringUtils.EscapeXML(meta.domain)).append("</dom>\n");
            xml.append("<hos>").append(StringUtils.EscapeXML(meta.host)).append("</hos>\n");
            xml.append("<pr>").append(meta.pageRank).append("</pr>\n");
            xml.append("<pa>").append(meta.pageAuthority).append("</pa>\n");
            xml.append("<udp>").append(meta.urlDepth).append("</udp>\n");
            xml.append("</sphinx:document>\n");
            return xml.toString();
        });

        result.saveAsTextFile(args[1], GzipCodec.class);

        System.out.printf("Docs: %,d \n", docCount.value());
        System.out.println("Time: " + stopwatch);


        return;

    }
}
