package org.atesh.hawk;

import com.google.common.base.Stopwatch;
import com.google.common.hash.Hashing;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.spark.Accumulator;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.storage.StorageLevel;
import org.atesh.commons.docs.SeyyedDocV1;
import org.atesh.commons.url.URLUtils;
import org.msgpack.MessagePack;
import org.msgpack.template.Templates;
import scala.Tuple2;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class DuplicateAnalyzer {

    public static void main(String[] args) throws IOException {

        String appName = "Duplicate Analyzer";
        SparkConf conf = new SparkConf().setAppName(appName);
        conf.set("spark.executor.memory", "24g");
        conf.set("spark.serializer", "org.apache.spark.serializer.KryoSerializer");
        JavaSparkContext sc = new JavaSparkContext(conf);

        Stopwatch stopwatch = Stopwatch.createStarted();

        Accumulator<Integer> docCount = sc.accumulator(0);
        JavaPairRDD<LongWritable, BytesWritable> rows = sc.sequenceFile(args[0], LongWritable.class, BytesWritable.class);


        rows = rows.coalesce(5000);
        final long RAW_HASH = 1l;
        final long WHITE_HASH = 2l;
        final long DIGIT_WHITE_HASH = 3l;
        final long LETTER_HASH = 4l;


        JavaPairRDD<String, Long> pairs = rows.flatMapToPair(t -> {
            List<Tuple2<String, Long>> res = new ArrayList<Tuple2<String, Long>>();
            docCount.add(1);
            MessagePack msgpck = new MessagePack();
            Map<String, String> map = msgpck.read(t._2.getBytes(), Templates.tMap(Templates.TString, Templates.TString));
            SeyyedDocV1 sdoc = SeyyedDocV1.fromMap(map);

            String text = sdoc.title + sdoc.content + sdoc.inboundAnchor + sdoc.keywords + sdoc.machineKeywords;
            String hash = URLUtils.getHostName(sdoc.originalURL) + Hashing.sha1().hashString(text, Charset.defaultCharset()).toString();
            res.add(new Tuple2(hash, RAW_HASH));

            text = text.replaceAll("\\s+", "");
            hash = URLUtils.getHostName(sdoc.originalURL) + Hashing.sha1().hashString(text, Charset.defaultCharset()).toString();
            res.add(new Tuple2(hash, WHITE_HASH));

            text = text.replaceAll("\\d+", "");
            hash = URLUtils.getHostName(sdoc.originalURL) + Hashing.sha1().hashString(text, Charset.defaultCharset()).toString();
            res.add(new Tuple2(hash, DIGIT_WHITE_HASH));


            text = text.replaceAll("\\P{L}+", "");
            hash = URLUtils.getHostName(sdoc.originalURL) + Hashing.sha1().hashString(text, Charset.defaultCharset()).toString();
            res.add(new Tuple2(hash, LETTER_HASH));

            return res;
        });

        pairs.persist(StorageLevel.MEMORY_AND_DISK());

        long raws = pairs.filter(t -> t._2 == RAW_HASH).map(t -> t._1).distinct(500).count();
        long whites = pairs.filter(t -> t._2 == WHITE_HASH).map(t -> t._1).distinct(500).count();
        long digits = pairs.filter(t -> t._2 == DIGIT_WHITE_HASH).map(t -> t._1).distinct(500).count();
        long letters = pairs.filter(t -> t._2 == LETTER_HASH).map(t -> t._1).distinct(500).count();

        System.out.println("Done! " + stopwatch.toString());
        System.out.printf("Number of docs: %,d %n", docCount.value());
        System.out.printf("Number of raws: %,d %n", raws);
        System.out.printf("Number of whites: %,d %n", whites);
        System.out.printf("Number of digits: %,d %n", digits);
        System.out.printf("Number of letters: %,d %n", letters);

    }
}