package org.atesh.hawk;

import com.google.common.base.CharMatcher;
import com.google.common.base.Stopwatch;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.storage.StorageLevel;
import org.atesh.commons.docs.SeyyedDocV1;
import org.atesh.commons.text.PersoLatinCharactarFilter;
import org.atesh.commons.text.SimpleTokenizer;
import org.atesh.commons.text.SoftNormalizer;
import scala.Tuple2;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class NGrams {

    public static void main(String[] args) {
        String appName = "Ngrams";
        SparkConf conf = new SparkConf().setAppName(appName);
        conf.set("spark.executor.memory", "24g");
        conf.set("spark.serializer", "org.apache.spark.serializer.KryoSerializer");
        JavaSparkContext sc = new JavaSparkContext(conf);

        Stopwatch stopwatch= Stopwatch.createStarted();

//
//        JavaPairRDD<String, Long> rows = sc.textFile("hdfs://psh-master:9000/dict/words/*").mapToPair(
//                t -> {
//                    String[] tokens = t.replace('(', ' ').replace(')', ' ').trim().split(",");
//                    return new Tuple2<String, Long>(tokens[1], Long.parseLong(tokens[0]));
//                }
//        );
//
//        Tuple2<String, Long> ss = rows.first();
//
//        List<Long> dict = rows.map(t -> Hashing.murmur3_128().hashString(t._1, Charset.defaultCharset()).asLong()).collect();
//
//
//        JavaRDD<Object> pairs = sc.objectFile(args[0]).coalesce(1000);
//        System.out.printf("Dictionary: %,d \n", dict.size());
//        System.out.print(pairs.count());


        JavaPairRDD<LongWritable, Text> rows = sc.sequenceFile(args[0], LongWritable.class, Text.class);
        rows = rows.coalesce(5000);


        JavaPairRDD<String, Long> pairs = rows.flatMapToPair(t -> {
            SeyyedDocV1 sdoc = SeyyedDocV1.fromJson(t._2.toString());
            String content = sdoc.content;

            if (content == null)
                return new ArrayList<Tuple2<String, Long>>();

            content = new SoftNormalizer().normalize(content);
            content = new PersoLatinCharactarFilter().filterToSpace(content);
            List<String> tokens = new SimpleTokenizer().tokenize(content);

            HashSet<String> ngrams = new HashSet<String>();
            String before = null;
            String before2 = null;
            for (String token : tokens) {
                ngrams.add(token);
                if (before != null) {
                    ngrams.add(before + "_" + token);
                    if (before2 != null)
                        ngrams.add(before2 + "_" + before + "_" + token);
                }
                before2 = before;
                before = token;
            }


            ArrayList<Tuple2<String, Long>> list = new ArrayList<Tuple2<String, Long>>();

            for (String str : ngrams) {
                list.add(new Tuple2<String, Long>(str, 1l));
            }
            return list;
        });

        JavaPairRDD<String, Long> reduced = pairs.reduceByKey((a, b) -> a + b, 500);
        //alaki
        reduced.persist(StorageLevel.DISK_ONLY());

        JavaPairRDD<String, Long> one_grams = reduced.filter(t -> t._2 >= 50 && CharMatcher.is('_').countIn(t._1) == 0);
        long ongrams_count = one_grams.count();
        JavaPairRDD<Long, String> output = one_grams.mapToPair(t -> new Tuple2<Long, String>(t._2, t._1)).sortByKey(false, 100);
        output.saveAsTextFile(args[1] + "//1");

        JavaPairRDD<String, Long> bigrams = reduced.filter(t -> t._2 >= 50 && CharMatcher.is('_').countIn(t._1) == 1);
        long bigrams_count = bigrams.count();
        output = bigrams.mapToPair(t -> new Tuple2<Long, String>(t._2, t._1)).sortByKey(false, 100);
        output.saveAsTextFile(args[1] + "//2");

        JavaPairRDD<String, Long> trigrams = reduced.filter(t -> t._2 >= 50 && CharMatcher.is('_').countIn(t._1) == 2);
        long trigrams_count = trigrams.count();
        output = trigrams.mapToPair(t -> new Tuple2<Long, String>(t._2, t._1)).sortByKey(false, 100);
        output.saveAsTextFile(args[1] + "//3");



        System.out.println("Done! "+stopwatch.toString());
        System.out.printf("Number of words: %,d \n", ongrams_count);
        System.out.printf("Number of bigrams: %,d \n", bigrams_count);
        System.out.printf("Number of trigrams: %,d \n", trigrams_count);
    }
}