package org.atesh.reader;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.KeyValue;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.hbase.mapreduce.TableMapper;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.atesh.commons.util.ShitID;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NavigableMap;


public class RankerReader extends Configured implements Tool {

    private static final byte[] META_DATA = Bytes.toBytes("M");
    private static final byte[] ANCHOR_BYTES = Bytes.toBytes("A");
    private static final byte[] O_BYTES = Bytes.toBytes("O"); // outlink urls

    public static void main(String[] args) throws Exception {
        int res = ToolRunner.run(new Configuration(), new RankerReader(), args);
        System.exit(res);
    }

    @Override
    public int run(String[] args) throws Exception {
        Configuration conf = HBaseConfiguration.create();
        conf.set("hbase.zookeeper.quorum", "rnk-1,rnk-2,rnk-3");
        conf.set("hbase.zookeeper.property.clientPort", "2181");
        conf.set("mapred.task.timeout", "3600000");
        conf.set("mapred.reduce.max.attempts", "10");
        conf.set("dfs.replication", "1");
        conf.set("mapred.output.compress", "true");
        conf.set("mapred.output.compression.codec", "org.apache.hadoop.io.compress.GzipCodec");

        Job job = new Job(conf, "Export Ranker Graph to Gson");
        job.setJarByClass(RankerReader.class);

        Scan scan = new Scan();
        scan.setCaching(100);
        scan.setCacheBlocks(false);
        scan.addFamily(ANCHOR_BYTES);
        scan.addFamily(META_DATA);
        scan.addFamily(O_BYTES);
        // scan.setStopRow(Bytes.toBytes((long) Math.pow(2, 42)));
        // scan.setStartRow(Bytes.toBytes((long) 0));

        // Mapper
        TableMapReduceUtil.initTableMapperJob("t", scan, ExportMapper.class, Text.class, Text.class, job);

        job.setOutputFormatClass(org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat.class);

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);
        job.setNumReduceTasks(0);
        FileOutputFormat.setOutputPath(job, new Path("gson"));
        return job.waitForCompletion(true) ? 0 : 1;

    }

    static class ExportMapper extends TableMapper<Text, Text> {

        @Override
        protected void setup(Context context) throws IOException, InterruptedException {

        }

        protected void cleanup(Context context) throws IOException, InterruptedException {

        }

        @Override
        public void map(ImmutableBytesWritable row, Result values, Context context) throws IOException {
            Map<String, String> urlToAnchor = new HashMap<String, String>();
            Map<String, String> metaKey = new HashMap<String, String>();
            Map<Long, String> idToUrl = new HashMap<Long, String>();
            KeyValue sourceUrlKV = values.getColumnLatest(META_DATA, "url".getBytes());
            KeyValue crawlopicKV = values.getColumnLatest(META_DATA, "crawlopic".getBytes());
            KeyValue languageKV = values.getColumnLatest(META_DATA, "language".getBytes());
            KeyValue redirectTolKV = values.getColumnLatest(META_DATA, "redirectTo".getBytes());

            if (sourceUrlKV == null) {
                context.getCounter("Source URLs", "Null").increment(1);
                return;
            }

            Gson gson = new GsonBuilder().disableHtmlEscaping().create();

            metaKey.put("url", Bytes.toString(sourceUrlKV.getValue()));
            if (crawlopicKV != null)
                metaKey.put("opic", Double.toString(Bytes.toDouble(crawlopicKV.getValue())));
            if (languageKV != null)
                metaKey.put("lang", Bytes.toString(languageKV.getValue()));
            if (redirectTolKV != null)
                metaKey.put("redir", Bytes.toString(redirectTolKV.getValue()));

            // extract outlink urls ind Internal/Follows
            // NavigableMap<byte[], byte[]> metas =
            // values.getFamilyMap(META_DATA);
            NavigableMap<byte[], byte[]> outlinkUrls = values.getFamilyMap(O_BYTES);
            NavigableMap<byte[], byte[]> outlinksAnchors = values.getFamilyMap(ANCHOR_BYTES); // Anchor
            // texts

            try {
                for (Entry<byte[], byte[]> entry : outlinkUrls.entrySet()) {

                    String url = Bytes.toString(entry.getKey());
                    long docId = ShitID.create(url);
                    // String url = Bytes.toString(entry.getValue());
                    idToUrl.put(docId, url);
                    urlToAnchor.put(url, "");

                }

                for (Entry<byte[], byte[]> entry : outlinksAnchors.entrySet()) {

                    byte[] docId_Destination_Bytes = entry.getKey(); // docId of
                    // destination
                    // url
                    long docId_Destination = Bytes.toLong(docId_Destination_Bytes);

                    String anchor = Bytes.toString(entry.getValue());

                    if (idToUrl.containsKey(docId_Destination)) {
                        urlToAnchor.put(idToUrl.get(docId_Destination), anchor);
                    }

                }
                context.getCounter("Source URLs", "Process").increment(1);
                context.write(new Text(gson.toJson(metaKey)), new Text(gson.toJson(urlToAnchor)));

            } catch (Throwable e) {
                context.getCounter("Source URLs", "Errors").increment(1);
            }
        }

    }

}
