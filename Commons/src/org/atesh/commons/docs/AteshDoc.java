package org.atesh.commons.docs;

import com.google.gson.GsonBuilder;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.WritableUtils;
import org.atesh.commons.io.MappableType;
import org.atesh.commons.text.AlphaNumericCharactarFilter;
import org.atesh.commons.text.SoftNormalizer;
import org.atesh.commons.text.StringUtils;
import org.atesh.commons.url.URLUtils;
import org.atesh.commons.util.WordRepeatSerializer;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by saeed on 1/29/15.
 */
public class AteshDoc extends MappableType implements Serializable, Writable {
    public final static int MAX_URL_LENGTH = 256;
    public final static int MAX_HOST_LEVELS = 5;   //means dots in host
    public final static int MAX_TITLE_LENGTH = 256;
    public final static int MAX_HEADINGS_LENGTH = 2048;
    public final static int MAX_CONTENT_LENGTH = 256 * 1024;
    public final static int MAX_ANCHOR_LENGTH = 5 * 1024 * 1024;

    public final static int MAX_TITLE_SNIPPET_LENGTH = 256;

    private static final long serialVersionUID = -8193794312340860093L;
    private static final short VERSION = 1;
    public long id = -1;
    public long baghali = 0;
    public String url = "";
    public String title = "";
    public String content = "";
    public String headings = "";
    public String anchors = "";
    public String host = "";
    public float pageRank = 0.0f;
    public float pageAuthority = 0.0f;
    public long timestamp = 0l;

    private AteshDoc() {
    }

    public static AteshDoc fromSeyyedDocV1(SeyyedDocV1 sdoc) throws MalformedURLException {
        AteshDoc doc = new AteshDoc();
        doc.baghali = sdoc.id;
        doc.url = sdoc.originalURL;
        doc.title = sdoc.title;
        doc.content = sdoc.content;
        doc.headings = sdoc.headings;
        doc.anchors = sdoc.machineKeywords;
        doc.host = URLUtils.getHostName(doc.url);
        doc.timestamp = sdoc.timestamp;
        doc.pageRank = (float) sdoc.pageRank;
        doc.pageAuthority = sdoc.pq_pageAuthority;
        return doc;
    }

    public Map<String, Integer> getAnchors() throws IOException {
        return WordRepeatSerializer.deserialize(anchors);
    }

    public String toJson() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("bgh", Double.toString(baghali));
        map.put("url", url);
        map.put("tit", title);
        map.put("hdr", headings);
        map.put("cnt", content);
        map.put("anc", anchors);
        map.put("hos", host);
        map.put("pr", Double.toString(pageRank));
        map.put("pa", Double.toString(pageAuthority));
        return new GsonBuilder().disableHtmlEscaping().create().toJson(map);
    }

    @Override
    public void write(DataOutput dataOutput) throws IOException {
        dataOutput.writeShort(VERSION);
        if (VERSION == 1) {
            dataOutput.writeLong(id);
            dataOutput.writeLong(baghali);
            WritableUtils.writeString(dataOutput, url);
            WritableUtils.writeString(dataOutput, title);
            WritableUtils.writeString(dataOutput, content);
            WritableUtils.writeString(dataOutput, headings);
            WritableUtils.writeString(dataOutput, anchors);
            WritableUtils.writeString(dataOutput, host);
            dataOutput.writeLong(timestamp);
            dataOutput.writeFloat(pageRank);
            dataOutput.writeFloat(pageAuthority);
        }
    }

    @Override
    public void readFields(DataInput dataInput) throws IOException {
        int version = dataInput.readShort();
        if (version == 1) { // Here we read
            id = dataInput.readLong();
            baghali = dataInput.readLong();
            url = WritableUtils.readString(dataInput);
            title = WritableUtils.readString(dataInput);
            content = WritableUtils.readString(dataInput);
            headings = WritableUtils.readString(dataInput);
            anchors = WritableUtils.readString(dataInput);
            host = WritableUtils.readString(dataInput);
            timestamp = dataInput.readLong();
            pageRank = dataInput.readFloat();
            pageAuthority = dataInput.readFloat();
        }
    }

    public void stripContent() {
        content = "";
    }

    public void expandAnchors() {
        if (anchors.length() == 0)
            return;
        int length = MAX_ANCHOR_LENGTH / anchors.length();
        anchors = WordRepeatSerializer.toRepeatString(anchors, length);
    }


    public boolean isGoodForXml() {
        return url.length() < MAX_URL_LENGTH &&
                !StringUtils.hasControl(url) &&
                !StringUtils.hasSurrogate(url) &&
                URLUtils.getHostNameLevels(host) < MAX_HOST_LEVELS &&
                title.length() < MAX_TITLE_LENGTH;
    }

    public void normalize() {
        SoftNormalizer normalizer = new SoftNormalizer();
        title = normalizer.normalize(title);
        content = normalizer.normalize(content);
        headings = normalizer.normalize(headings);
        anchors = normalizer.normalize(anchors);
    }

    public void removeNonAlphaNumeric() {
        AlphaNumericCharactarFilter filter = new AlphaNumericCharactarFilter();
        title = StringUtils.white2Space(filter.filterToSpace(title));
        content = StringUtils.white2Space(filter.filterToSpace(content));
        headings = StringUtils.white2Space(filter.filterToSpace(headings));
        anchors = StringUtils.white2Space(filter.filterToSpace(anchors));
    }


    public void shave() {
        content = content.substring(0, Math.min(content.length(), MAX_CONTENT_LENGTH));
        headings = headings.substring(0, Math.min(headings.length(), MAX_HEADINGS_LENGTH));
        anchors = anchors.substring(0, Math.min(anchors.length(), MAX_ANCHOR_LENGTH));
    }

    public String toSphinxXml() throws MalformedURLException {
        expandAnchors();
        shave();

        String sit = StringUtils.clearUTF(title.substring(0, Math.min(title.length(), MAX_TITLE_SNIPPET_LENGTH)));
        String domain = StringUtils.clearUTF(URLUtils.getDomain(url));
        host = StringUtils.clearUTF(host);
        normalize();
        removeNonAlphaNumeric();


        StringBuilder xml = new StringBuilder(content.length() * 11 / 10);
        xml.append("<sphinx:document id=\"").append(id).append("\">\n");
        xml.append("<baghali>").append(baghali).append("</baghali>\n");
        xml.append("<url>").append(StringUtils.EscapeXML(url)).append("</url>\n");
        xml.append("<sit>").append(StringUtils.EscapeXML(sit)).append("</sit>\n");
        xml.append("<tit>").append(StringUtils.EscapeXML(title)).append("</tit>\n");
        xml.append("<hdr>").append(StringUtils.EscapeXML(headings)).append("</hdr>\n");
        xml.append("<cnt>").append(StringUtils.EscapeXML(content)).append("</cnt>\n");
        xml.append("<anc>").append(StringUtils.EscapeXML(anchors)).append("</anc>\n");
        xml.append("<dom>").append(StringUtils.EscapeXML(domain)).append("</dom>\n");
        xml.append("<hos>").append(StringUtils.EscapeXML(host)).append("</hos>\n");
        xml.append("<pr>").append(pageRank).append("</pr>\n");
        xml.append("<pa>").append(pageAuthority).append("</pa>\n");
        xml.append("<tim>").append(timestamp / 1000).append("</tim>\n");
        xml.append("</sphinx:document>\n");
        return xml.toString();
    }


    @Override
    public void fromMap(Map<String, String> map) {
    }

    @Override
    public Map<String, String> toMap() {
        return null;
    }
}
