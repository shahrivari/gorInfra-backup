package org.atesh.commons.docs;

//import ir.nse.normalizer.AteshNormalizer;

import com.google.gson.GsonBuilder;
import org.atesh.commons.url.URLUtils;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by saeed on 9/25/14.
 */
public class AteshDocV2 implements Serializable {
    public final static int MAX_TITLE = 256;
    public final static int MAX_HEAD = 2048;
    public final static int MAX_CNT = 1024 * 1024;
    private static final long serialVersionUID = -2402135660617201510L;
    public long id = -1;
    public long baghali = 0;
    public String url = "";
    public String title = "";
    public String content = "";
    public String headings = "";
    public String anchors = "";
    public String domain = "";
    public String host = "";
    public double pageRank = 0.0;
    public double pageAuthority = 0.0;
    public double urlDepth = 0.0;

    private AteshDocV2() {
    }

    public static AteshDocV2 fromSeyyedDocV1(SeyyedDocV1 sdoc) throws MalformedURLException {
        AteshDocV2 doc = new AteshDocV2();
        doc.baghali = sdoc.id;
        doc.url = sdoc.originalURL;
        doc.title = sdoc.title;
        doc.content = sdoc.content;
        doc.headings = sdoc.headings;
        doc.anchors = sdoc.machineKeywords;
        doc.domain = URLUtils.getDomain(doc.url);
        doc.host = URLUtils.getHostName(doc.url);
        doc.pageRank = sdoc.pageRank;
        doc.pageAuthority = sdoc.pq_pageAuthority;
        doc.urlDepth = sdoc.pq_urlDepth;
        return doc;
    }

    public String toJson() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("id", Double.toString(baghali));
        map.put("url", url);
        map.put("tit", title);
        map.put("hdr", headings);
        map.put("cnt", content);
        map.put("anc", anchors);
        map.put("hos", host);
        map.put("pr", Double.toString(pageRank));
        map.put("pa", Double.toString(pageAuthority));
        return new GsonBuilder().disableHtmlEscaping().create().toJson(map);
    }

}
