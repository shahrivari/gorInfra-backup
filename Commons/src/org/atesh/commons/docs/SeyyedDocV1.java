package org.atesh.commons.docs;

import com.google.common.hash.Hashing;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.atesh.commons.url.URLUtils;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.nio.charset.Charset;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by saeed on 9/18/14.
 */
public class SeyyedDocV1 implements Serializable {
    public long id = 0;
    public String originalURL = "";
    public String title = "";
    public String content = "";
    public String mainContent = "";
    public String headings = "";
    public int category = 0;
    public String domain = "";
    public String url = "";
    public String keywords = "";
    public String machineKeywords = "";
    public long timestamp = 0;

    public String inboundAnchor = "";
    public String mainContentHash = "";
    public double pageRank = 0.0;

    public float pq_advertRate = 0.0f;
    public float pq_entropy = 0.0f;
    public float pq_fracAnchorText = 0.0f;
    public float pq_fracStop = 0.0f;
    public float pq_fracTableText = 0.0f;
    public float pq_numTitleTerms = 0.0f;
    public float pq_pageAuthority = 0.0f;
    public float pq_pageStatus = 0.0f;
    public float pq_stopCover = 0.0f;
    public float pq_termLength = 0.0f;
    public float pq_urlDepth = 0.0f;
    public float spam_metaPrediction = 0.0f;
    public float spam_spc = 0.0f;

    public static SeyyedDocV1 fromMap(Map<String, String> map) {
        SeyyedDocV1 doc = new SeyyedDocV1();
        for (Map.Entry<String, String> entry : map.entrySet()) {
            String key = entry.getKey().toLowerCase();
            if (key.equals("id".toLowerCase()))
                doc.id = Long.parseLong(entry.getValue());
            else if (key.equals("originalURL".toLowerCase()))
                doc.originalURL = entry.getValue();
            else if (key.equals("title".toLowerCase()))
                doc.title = entry.getValue();
            else if (key.equals("content".toLowerCase()))
                doc.content = entry.getValue();
            else if (key.equals("mainContent".toLowerCase()))
                doc.mainContent = entry.getValue();
            else if (key.equals("headings".toLowerCase()))
                doc.headings = entry.getValue();
            else if (key.equals("category".toLowerCase()))
                doc.category = Integer.parseInt(entry.getValue());
            else if (key.equals("domain".toLowerCase()))
                doc.domain = entry.getValue();
            else if (key.equals("url".toLowerCase()))
                doc.url = entry.getValue();
            else if (key.equals("keywords".toLowerCase()))
                doc.keywords = entry.getValue();
            else if (key.equals("machineKeywords".toLowerCase()))
                doc.machineKeywords = entry.getValue();
            else if (key.equals("timestamp".toLowerCase()))
                doc.timestamp = Long.parseLong(entry.getValue());
            else if (key.equals("inboundAnchor".toLowerCase()))
                doc.inboundAnchor = entry.getValue();
            else if (key.equals("mainContentHash".toLowerCase()))
                doc.mainContentHash = entry.getValue();
            else if (key.equals("pageRank".toLowerCase()))
                doc.pageRank = Double.parseDouble(entry.getValue());
            else if (key.equals("pq_advertRate".toLowerCase()))
                doc.pq_advertRate = Float.parseFloat(entry.getValue());
            else if (key.equals("pq_entropy".toLowerCase()))
                doc.pq_entropy = Float.parseFloat(entry.getValue());
            else if (key.equals("pq_fracAnchorText".toLowerCase()))
                doc.pq_fracAnchorText = Float.parseFloat(entry.getValue());
            else if (key.equals("pq_fracStop".toLowerCase()))
                doc.pq_fracStop = Float.parseFloat(entry.getValue());
            else if (key.equals("pq_fracTableText".toLowerCase()))
                doc.pq_fracTableText = Float.parseFloat(entry.getValue());
            else if (key.equals("pq_numTitleTerms".toLowerCase()))
                doc.pq_numTitleTerms = Float.parseFloat(entry.getValue());
            else if (key.equals("pq_pageAuthority".toLowerCase()))
                doc.pq_pageAuthority = Float.parseFloat(entry.getValue());
            else if (key.equals("pq_pageStatus".toLowerCase()))
                doc.pq_pageStatus = Float.parseFloat(entry.getValue());
            else if (key.equals("pq_stopCover".toLowerCase()))
                doc.pq_stopCover = Float.parseFloat(entry.getValue());
            else if (key.equals("pq_termLength".toLowerCase()))
                doc.pq_termLength = Float.parseFloat(entry.getValue());
            else if (key.equals("pq_urlDepth".toLowerCase()))
                doc.pq_urlDepth = Float.parseFloat(entry.getValue());
            else if (key.equals("spam_metaPrediction".toLowerCase()))
                doc.spam_metaPrediction = Float.parseFloat(entry.getValue());
            else if (key.equals("spam_spc".toLowerCase()))
                doc.spam_spc = Float.parseFloat(entry.getValue());
        }

        if (doc.id == 0)
            throw new IllegalArgumentException("The given map has no id.");
        if (doc.originalURL == "")
            throw new IllegalArgumentException("The given map has no url.");

        return doc;
    }

    public static SeyyedDocV1 fromJson(String json) {
        Gson gson = new GsonBuilder().create();
        Map<String, String> map = gson.fromJson(json, Map.class);
        return fromMap(map);
    }

    public String toFetchJson() {
        Gson gson = new GsonBuilder().disableHtmlEscaping().create();
        LinkedHashMap<String, String> map = new LinkedHashMap<String, String>(10);
        map.put("id", Long.toString(id));
        map.put("url", originalURL);
        map.put("ttl", title.substring(0, Math.min(title.length(), 512)));
        map.put("cnt", content.substring(0, Math.min(content.length(), 512000)));
        map.put("mcn", mainContent.substring(0, Math.min(mainContent.length(), 512000)));
        map.put("cat", Integer.toString(category));
        return gson.toJson(map);
    }

    /*
    public AteshDoc toAteshDoc() {
        AteshDoc doc = new AteshDoc();
        doc.id = id;
        doc.url = originalURL;
        doc.tit = title;
        doc.hdr = headings;
        doc.cnt = content;
        doc.mcn = mainContent;
        doc.dom = domain;
        doc.anc = inboundAnchor;
        doc.kwd = keywords;
        doc.mkw = machineKeywords;
        doc.cat = category;
        doc.pr = (float) pageRank;
        doc.pa = pq_pageAuthority;
        doc.ukw = url;
        doc.udp = pq_urlDepth;
        doc.ent = pq_entropy;
        doc.fat = pq_fracAnchorText;
        doc.ftt = pq_fracTableText;
        doc.fsw = pq_fracStop;
        doc.atl = pq_termLength;
        return doc;
    }
*/
    public String getFigerPrint() throws MalformedURLException {
        String all = title + content + headings + inboundAnchor + keywords + machineKeywords;
        all = all.replaceAll("\\d+", "").replaceAll("\\s+", "");
        return URLUtils.getHostName(originalURL) + Hashing.sha1().hashString(all, Charset.defaultCharset()).toString();
    }
}
