package org.atesh.commons.docs;

import org.atesh.commons.text.AteshNormalizer;
import org.atesh.commons.text.PersoLatinCharactarFilter;
import org.atesh.commons.text.StringUtils;
import org.atesh.commons.util.WordRepeatSerializer;

import java.io.Serializable;

/**
 * Created by saeed on 1/21/15.
 */
public class AteshMetaDoc implements Serializable {
    public final static int MAX_TITLE = 200;
    public final static int MAX_HEAD = 1000;
    private static final long serialVersionUID = 1898553853572525928L;
    public long id = -1;
    public long baghali = 0;
    public String url = "";
    public String title = "";
    public String headings = "";
    public String anchors = "";
    public String domain = "";
    public String host = "";
    public double pageRank = 0.0;
    public double pageAuthority = 0.0;
    public double urlDepth = 0.0;

    public static AteshMetaDoc fromAteshDocV2(AteshDocV2 doc) {
        AteshMetaDoc meta = new AteshMetaDoc();
        meta.id = doc.id;
        meta.baghali = doc.baghali;
        meta.url = doc.url;
        meta.title = doc.title;
        meta.headings = doc.headings;
        meta.anchors = doc.anchors;
        meta.domain = doc.domain;
        meta.host = doc.host;
        meta.pageRank = doc.pageRank;
        meta.pageAuthority = doc.pageAuthority;
        meta.urlDepth = doc.urlDepth;
        return meta;
    }

    public boolean isGood() {
        return title.length() < MAX_TITLE && headings.length() < MAX_HEAD;
    }


    public void refine() {
        AteshNormalizer normalizer = new AteshNormalizer();
        PersoLatinCharactarFilter filter = new PersoLatinCharactarFilter();
        title = StringUtils.white2Space(AteshNormalizer.normalize(filter.filterToSpace(title)));
        headings = StringUtils.white2Space(AteshNormalizer.normalize(filter.filterToSpace(headings)));
        anchors = StringUtils.white2Space(AteshNormalizer.normalize(filter.filterToSpace(WordRepeatSerializer.toRepeatString(anchors, 1024))));
        domain = StringUtils.clearUTF(domain);
        host = StringUtils.clearUTF(host);
        url = StringUtils.clearUTF(url);
    }

}
