package org.atesh.commons.docs;

//import ir.nse.normalizer.AteshNormalizer;

import org.atesh.commons.util.WordRepeatSerializer;

/**
 * Created by saeed on 9/25/14.
 */
public class AteshDocOLD {
    public final static int MAX_TITLE = 256;
    public final static int MAX_HEAD = 1024;
    public final static int MAX_CNT = 1024 * 1024;
    public static final int maxAnchorRepeat = 2048;
    public long id;
    public String url;
    public String tit;
    public String cnt;
    public String mcn;
    public String hdr;
    public String anc;
    public String kwd;
    public String mkw;
    public String ukw;
    public String dom;
    public int cat;
    public float pr;
    public float pa;
    public float udp;
    public float ent;
    public float fat;
    public float ftt;
    public float atl;
    public float fsw;

    public void adjustPageRank(float f) {
        if (pr < f)
            pr = f;
    }

    public AteshDocOLD expandAnchors() {
        anc = WordRepeatSerializer.toRepeatString(anc, maxAnchorRepeat);
        kwd = WordRepeatSerializer.toRepeatString(kwd, maxAnchorRepeat);
        mkw = WordRepeatSerializer.toRepeatString(mkw, maxAnchorRepeat);
        return this;
    }


//    public AteshDoc normalize() {
//        tit = AteshNormalizer.normalizeString(tit);
//        cnt = AteshNormalizer.normalizeString(cnt);
//        mcn = AteshNormalizer.normalizeString(mcn);
//        hdr = AteshNormalizer.normalizeString(hdr);
//        anc = AteshNormalizer.normalizeString(anc);
//        kwd = AteshNormalizer.normalizeString(kwd);
//        mkw = AteshNormalizer.normalizeString(mkw);
//        ukw = AteshNormalizer.normalizeString(ukw);
//        dom = AteshNormalizer.normalizeString(dom);
//        return this;
//    }

    public AteshDocOLD shave() {
        if (tit.length() > MAX_TITLE)
            tit = tit.substring(0, MAX_TITLE) + "...";
        if (hdr.length() > MAX_HEAD)
            hdr = hdr.substring(0, MAX_HEAD) + "...";
        if (cnt.length() > MAX_CNT)
            cnt = cnt.substring(0, MAX_CNT) + "...";
        if (mcn.length() > MAX_CNT)
            mcn = mcn.substring(0, MAX_CNT) + "...";

        return this;
    }


}
