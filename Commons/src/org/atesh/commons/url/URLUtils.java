package org.atesh.commons.url;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;

/**
 * Created with IntelliJ IDEA.
 * User: saeed
 * Date: 10/31/13
 * Time: 4:02 PM
 * To change this template use File | Settings | File Templates.
 */
public class URLUtils {

    public static String clean(String url_str) throws MalformedURLException, UnsupportedEncodingException {
        url_str = url_str.toLowerCase().trim();

        if (!url_str.startsWith("http") && !url_str.startsWith("ftp"))
            throw new MalformedURLException("URl does not start with http or ftp!: " + url_str);

        if (url_str.length() < 5)
            throw new MalformedURLException("Short URL: " + url_str);

        //remove sections
        if (url_str.contains("#"))
            url_str = url_str.substring(0, url_str.lastIndexOf("#"));


        try {
            url_str = URLDecoder.decode(url_str, "UTF-8");
        } catch (IllegalArgumentException exp) {
            if (!exp.getMessage().contains("Illegal hex characters in escape (%) pattern") &&
                    !exp.getMessage().contains("Incomplete trailing escape (%) pattern"))
                throw exp;
        }

        URL url = new URL(url_str);

        url_str = url.toString();

        while (url_str.endsWith("/"))
            url_str = url_str.substring(0, url_str.length() - 1);

        if (url_str.length() < 10)
            throw new MalformedURLException("Short URL: " + url_str);


        return url_str;
    }

    public static String removewww(String url) {
        if (url.startsWith("http://www."))
            return url.replaceFirst("http://www\\.", "http://");
        if (url.startsWith("https://www."))
            return url.replaceFirst("https://www\\.", "https://");
        if (url.startsWith("ftp://www."))
            return url.replaceFirst("ftp://www\\.", "ftp://");
        return url;
    }

    public static String translateWhiteSpaces(String url) {
        //return  url.replaceAll("\\s","");
        return url.replaceAll(" ", "%20").replaceAll("\t", "%09").replaceAll("\n", "%0A");
    }


    public static String normalize(String url_str) throws MalformedURLException, UnsupportedEncodingException {
        url_str = clean(url_str);
        url_str = removewww(url_str);
        url_str = translateWhiteSpaces(url_str);
        return url_str;
    }

    static private final boolean isAsciLetter(char ch) {
        return (ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z');
    }

    static private final boolean isAsciDigit(char ch) {
        return (ch >= '0' && ch <= '9');
    }

    static private final boolean isValidHostChar(char ch) {
        return isAsciLetter(ch) || isAsciDigit(ch) || ch == '-' || ch == '.' || ch == '_' || ch == ':';
    }

    static public final boolean isValidHostName(String host) {
        host = host.trim();
        for (int i = 0; i < host.length(); i++)
            if (!isValidHostChar(host.charAt(i)))
                return false;
        if (host.startsWith("-") || host.endsWith("-"))
            return false;
        if (host.startsWith(".") || host.endsWith("."))
            return false;
        if (host.startsWith(":") || host.endsWith(":"))
            return false;

        int colon_count = 0;
        for (int i = 0; i < host.length(); i++)
            if (host.charAt(i) == ':')
                colon_count++;
        if (colon_count > 1)
            return false;

        if (host.endsWith("_"))
            return false;

        int dot_count = 0;
        for (int i = 0; i < host.length(); i++)
            if (host.charAt(i) == '.')
                dot_count++;
        if (dot_count == 0)
            return false;

        return true;
    }

    public static boolean isHomePage(String url) {
        if (!isValidURL(url))
            return false;

        int doubleslash = url.indexOf("//");
        if (doubleslash < 0)
            return false;

        int slash = url.indexOf('/', doubleslash);
        return slash >= 0;
    }

    public static boolean isValidURL(String url) {
        if (url.length() < 10)
            return false;
        url = url.trim();

        if (url.endsWith("%"))
            return false;

        if (url.startsWith("http://") || url.startsWith("https://") || url.startsWith("ftp://")) {
            String host = null;
            try {
                host = getHostName(url);
            } catch (MalformedURLException e) {
                return false;
            }

            if (!isValidHostName(host))
                return false;
            if (host.contains(".."))
                return false;
        } else {//unknown schema
            return false;
        }

        //it seems right
        return true;
    }


    public static String getHostName(String url) throws MalformedURLException {
        if (url == null || url.length() == 0)
            return "";

        url = url.toLowerCase();
        if (!url.startsWith("http://") && !url.startsWith("https://") && !url.startsWith("ftp://"))
            throw new MalformedURLException("Unknown schema!");

        int doubleslash = url.indexOf("//");
        doubleslash += 2;

        int end = url.length();

        int idx = url.indexOf('/', doubleslash);
        if (idx > 0 && idx < end)
            end = idx;

        idx = url.indexOf(':', doubleslash);
        if (idx > 0 && idx < end)
            end = idx;

        idx = url.indexOf('&', doubleslash);
        if (idx > 0 && idx < end)
            end = idx;

        idx = url.indexOf('=', doubleslash);
        if (idx > 0 && idx < end)
            end = idx;

        idx = url.indexOf('#', doubleslash);
        if (idx > 0 && idx < end)
            end = idx;

        idx = url.indexOf('?', doubleslash);
        if (idx > 0 && idx < end)
            end = idx;

        return url.substring(doubleslash, end);
    }

    public static int getHostNameLevels(String hostname) {
        int levels = 0;
        for (int i = 0; i < hostname.length(); i++)
            if (hostname.charAt(i) == '.')
                levels++;
        return levels;
    }


    public static String getDomain(String host) throws MalformedURLException {
        int dot_count = 0;
        for (int i = 0; i < host.length(); i++)
            if (host.charAt(i) == '.')
                dot_count++;
        if (dot_count == 0)
            throw new MalformedURLException("Host name without a dot!!!");
        if (dot_count < 2)
            return host;

        int last_dot = host.lastIndexOf('.');
        int before_last_dot = host.lastIndexOf('.', last_dot - 1);
        int before_before_last_dot = host.lastIndexOf('.', before_last_dot - 1);
        String domain = host.substring(before_last_dot + 1);
        if (last_dot - before_last_dot > 3) {
            if (!domain.startsWith("gov.") && !domain.startsWith("net.") &&
                    !domain.startsWith("org.") && !domain.startsWith("sch.") &&
                    !domain.startsWith("com.") && !domain.startsWith("edu.")
                    )
                return domain;
        }

        if (before_before_last_dot == -1)
            return host;

        domain = host.substring(before_before_last_dot + 1);
        return domain;
    }

    public static boolean isImage(String link) {
        return link.endsWith(".ico") || link.endsWith(".jpg") || link.endsWith(".jpeg") || link.endsWith(".png")
                || link.endsWith(".svg") || link.endsWith(".gif") || link.endsWith(".bmp");
    }

    public static String toKey(String url) throws MalformedURLException, UnsupportedEncodingException {
        String normal = clean(url);
        String domain_name = getHostName(normal);
        String result = normal;
        if (domain_name.startsWith("www."))
            result = result.replaceFirst("www\\.", "");
        return result;
    }


}
