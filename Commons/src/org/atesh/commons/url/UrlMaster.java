package org.atesh.commons.url;

import java.net.MalformedURLException;

/**
 * Created by saeed on 2/26/15.
 */
public class UrlMaster {
    String url = "";
    String cleanurl = "";
    String host = "";
    String path = "";
    String schema = "";

    public static UrlMaster wrap(String url) throws MalformedURLException {
        UrlMaster master = new UrlMaster();
        master.url = url;

        url = url.toLowerCase().trim();

        if (!url.startsWith("http") && !url.startsWith("ftp"))
            throw new MalformedURLException("URl does not start with http or ftp!: " + url);

        if (url.length() < 5)
            throw new MalformedURLException("Short URL: " + url);

        return master;
    }
}
