package org.atesh.commons.util;

import com.google.gson.Gson;
import org.atesh.commons.text.StringUtils;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

public class WordRepeatSerializer {

    public static LinkedHashMap<String, Integer> deserialize(String text) throws IOException {
        LinkedHashMap<String, Integer> result = new LinkedHashMap<String, Integer>();
        char[] charArray = text.toCharArray();
        int lastEnd = -1;
        boolean hasToken = false;
        for (int i = 0; i < charArray.length; i++) {
            char c = charArray[i];
            switch (c) {
                case ' ':
                    if (!hasToken)
                        lastEnd = i;
                    continue;
                case WordRepeatDetectorFilter.guardChar:
                    if (hasToken) {
                        lastEnd++;
                        int j = i - 1;
                        while (charArray[j] == ' ')
                            j--;
                        String token = new String(charArray, lastEnd, j - lastEnd + 1);
                        result.put(token, 1);
                    }

                    int startOffset = i;

                    i++;
                    if (i == charArray.length)
                        throw new IOException("Invalid serialized data: unclosed guard char at " + startOffset);
                    if (charArray[i] != ' ')
                        throw new IOException("Invalid serialized data: expected ' ' after openning guard char (" + startOffset + "), got '" + charArray[i] + "'");

                    while (charArray[i + 1] == ' ')
                        i++;

                    lastEnd = i + 1;

                    while (true) {
                        i++;
                        if (i == charArray.length)
                            throw new IOException("Invalid serialized data: unclosed guard char at " + startOffset);
                        c = charArray[i];
                        if (c == WordRepeatDetectorFilter.guardChar)
                            break;
                    }

                    String token;
                    if (lastEnd == i) {
                        token = "";
                    } else {
                        int j = i - 1;
                        if (charArray[j] != ' ')
                            throw new IOException("Invalid serialized data: expected ' ' before closing guard char (" + j + "), got '" + charArray[j] + "'");
                        j--;
                        while (charArray[j] == ' ')
                            j--;
                        token = new String(charArray, lastEnd, j - lastEnd + 1);
                    }

                    int endOffset = i;

                    i++;
                    if (i == charArray.length)
                        throw new IOException("Invalid serialized data: count char '" + WordRepeatDetectorFilter.countChar + "' not found after closing guard char (" + endOffset + ")");
                    if (charArray[i] != ' ')
                        throw new IOException("Invalid serialized data: expected ' ' after closing guard char (" + endOffset + "), got '" + charArray[i] + "'");

                    i++;
                    if (i == charArray.length)
                        throw new IOException("Invalid serialized data: count char '" + WordRepeatDetectorFilter.countChar + "' not found after closing guard char (" + endOffset + ")");
                    if (charArray[i] != WordRepeatDetectorFilter.countChar)
                        throw new IOException("Invalid serialized data: expected count char '" + WordRepeatDetectorFilter.countChar + "' after closing guard char (" + endOffset + ") and space, got '" + charArray[i] + "'");

                    i++;
                    if (i == charArray.length)
                        throw new IOException("Invalid serialized data: no count after count char (" + (i - 1) + ")");
                    if (charArray[i] != ' ')
                        throw new IOException("Invalid serialized data: expected ' ' after count char (" + (i - 1) + "), got '" + charArray[i] + "'");

                    int countStartOffsetMinusOne = i;

                    while (true) {
                        i++;
                        if (i == charArray.length)
                            throw new IOException("Invalid serialized data: expected space after count at " + countStartOffsetMinusOne);
                        c = charArray[i];
                        if (c == ' ')
                            break;
                    }

                    int repeatNum;
                    countStartOffsetMinusOne++;
                    String countString = new String(charArray, countStartOffsetMinusOne, i - countStartOffsetMinusOne);
                    try {
                        repeatNum = Integer.parseInt(countString);
                    } catch (NumberFormatException e) {
                        throw new IOException("Invalid serialized data: expected a number after count char, got '" + countString + "'");
                    }

                    result.put(token, repeatNum);

                    hasToken = false;
                    lastEnd = i;
                    break;
                default:
                    hasToken = true;
                    break;
            }
        }

        if (hasToken) {
            lastEnd++;
            int j = charArray.length - 1;
            while (charArray[j] == ' ') {
                j--;
            }
            String token = new String(charArray, lastEnd, j - lastEnd + 1);
            result.put(token, 1);
        }

        return result;
    }

    private static int digitSize(int digit) {
        int count = 0;
        while (digit != 0) {
            digit /= 10;
            count++;
        }
        return count;
    }


    public static String serialize(String text, int repeatNum) {
        return serialize(text, repeatNum, null);
    }

    public static String serialize(String text, int repeatNum, StringBuilder sb) {
        boolean shouldReturn = sb == null;

        if ((repeatNum <= 1) || (text.isEmpty()) || (text.trim().isEmpty())) {
            if (shouldReturn)
                return text;
            sb.append(text);
            return null;
        }

        if (repeatNum < 0)
            repeatNum *= -1;

        // check not having quotation or power sign
        text = text.replace(WordRepeatDetectorFilter.guardChar, ' ').replace(WordRepeatDetectorFilter.countChar, ' ');

        if (sb == null)
            sb = new StringBuilder(text.length() + 10 + digitSize(repeatNum));

        sb.append(' ');
        sb.append(WordRepeatDetectorFilter.guardChar);
        sb.append(' ');
        sb.append(text);
        sb.append(' ');
        sb.append(WordRepeatDetectorFilter.guardChar);
        sb.append(' ');
        sb.append(WordRepeatDetectorFilter.countChar);
        sb.append(' ');
        sb.append(repeatNum);
        sb.append(' ');

        if (shouldReturn)
            return sb.toString();
        return null;
    }

    public static String toRepeatString(String s, int max) {
        StringBuilder builder = new StringBuilder();
        LinkedHashMap<String, Integer> map = null;
        try {
            map = deserialize(s);
        } catch (IOException e) {
            return s;
        }

        for (Map.Entry<String, Integer> x : map.entrySet()) {
            int reps = Math.min(x.getValue(), max);
            if (x.getValue() > max)
                reps = max + (int) Math.sqrt(x.getValue());
            for (int i = 0; i < reps; i++)
                builder.append(x.getKey()).append(" ");
        }

        return builder.toString();
    }

    public static String toJson(String s) throws IOException {
        Gson gson = new Gson();
        s = StringUtils.white2Space(s);
        LinkedHashMap<String, Integer> map = deserialize(s);
        return gson.toJson(map);
    }


    class WordRepeatDetectorFilter {
        public final static char guardChar = '\"';
        public final static char countChar = '^';
    }


}

