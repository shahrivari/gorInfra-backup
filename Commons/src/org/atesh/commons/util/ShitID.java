package org.atesh.commons.util;

import com.planetj.math.rabinhash.RabinHashFunction64;
import org.apache.commons.codec.digest.DigestUtils;


/**
 * Created by saeed on 12/17/14.
 */
public class ShitID {
    public static long create(String value) {
        value = value.toLowerCase();
        String fingerprint = DigestUtils.md5Hex(DigestUtils.sha256Hex(value));
        long pageId = RabinHashFunction64.DEFAULT_HASH_FUNCTION.hash(fingerprint);
        pageId = Long.reverse(pageId);
        return pageId;
    }
}
