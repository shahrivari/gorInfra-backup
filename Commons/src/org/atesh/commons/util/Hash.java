package org.atesh.commons.util;

import com.google.common.hash.Hashing;

import java.nio.charset.Charset;

/**
 * Created by saeed on 9/25/14.
 */
public class Hash {
    public static int long2Bucket(long l, int buckets) {
        String md5 = Hashing.md5().hashString(Long.toString(l), Charset.defaultCharset()).toString().toLowerCase();
        return Integer.parseInt(md5.substring(0, 4), 16) % buckets;
    }
}
