package org.atesh.commons.io;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.Map;

/**
 * Created by Saeed on 1/31/2015.
 */
public abstract class MappableType {
    private final static byte BYTE_SIGN = 'B';
    private final static byte CHAR_SIGN = 'C';
    private final static byte SHORT_SIGN = 'H';
    private final static byte INT_SIGN = 'I';
    private final static byte LONG_SIGN = 'L';
    private final static byte FLOAT_SIGN = 'F';
    private final static byte DOUBLE_SIGN = 'D';
    private final static byte STRING_SIGN = 'S';
    private final static byte BYTES_SIGN = 'Y';
    private final static byte VSHORT_SIGN = (byte) 254;
    private final static byte VINT_SIGN = (byte) 255;
    private final static Charset UTF8_CHARSET = Charset.forName("UTF-8");

    private static void writeVInt(ByteBuffer buffer, int i) {
        if (i < 0)
            throw new IllegalArgumentException("The integer must be greater or equal to zero!: " + i);

        if (i <= 253) {
            buffer.put((byte) i);
            return;
        }

        if (i <= Short.MAX_VALUE) {
            buffer.put(VSHORT_SIGN);
            buffer.putShort((short) i);
            return;
        }

        buffer.put(VINT_SIGN);
        buffer.putInt(i);
    }

    private static int readVInt(ByteBuffer buffer) {
        byte val = buffer.get();
        if ((val & 0xFF) <= 253)
            return val;
        if (val == VSHORT_SIGN)
            return buffer.getShort();
        if (val == VINT_SIGN)
            return buffer.getInt();
        throw new IllegalStateException("This is impossible to happen!");
    }

    private static void writeString(ByteBuffer buffer, String str) {
        byte[] array = str.getBytes(UTF8_CHARSET);
        writeVInt(buffer, array.length);
        buffer.put(array);
    }

    public static byte[] toByteArray(Map<String, Object> map) {
        ByteBuffer buffer = ByteBuffer.allocate(64);
        buffer.putInt(map.size());
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            writeString(buffer, entry.getKey());
            Object val = entry.getValue();
            if (val instanceof String) {
                buffer.put(STRING_SIGN);
                writeString(buffer, (String) val);
            } else if (val instanceof Long) {
                buffer.put(LONG_SIGN);
                buffer.putLong((Long) val);
            } else if (val instanceof Integer) {
                buffer.put(INT_SIGN);
                buffer.putInt((Integer) val);
            } else if (val instanceof Short) {
                buffer.put(SHORT_SIGN);
                buffer.putShort((Short) val);
            } else if (val instanceof Character) {
                buffer.put(CHAR_SIGN);
                buffer.putChar((Character) val);
            } else if (val instanceof Byte) {
                buffer.put(BYTE_SIGN);
                buffer.put((Byte) val);
            } else if (val instanceof Float) {
                buffer.put(FLOAT_SIGN);
                buffer.putFloat((Float) val);
            } else if (val instanceof Double) {
                buffer.put(DOUBLE_SIGN);
                buffer.putDouble((Double) val);
            } else
                throw new IllegalArgumentException("The input map should just contain primitive types.");
        }

        return buffer.array();
    }

    public static void main(String[] args) {
        for (int i = 0; i < 256; i++) {
            byte b = (byte) i;
            int j = b & 0xFF;
            if (j != i)
                System.out.println("POOF: " + i + "<=>" + j);
        }

    }

    abstract public void fromMap(Map<String, String> map);

    abstract public Map<String, String> toMap();

}
