package org.atesh.commons.text;

/**
 * Created by Saeed on 1/6/2015.
 */
public abstract class ICharactarFilter {

    public static boolean isBasicLatin(char ch) {
        return ch >= '\u0021' && ch <= '\u007e';
    }

    public static boolean isSupplementLatin(char ch) {
        return ch >= '\u00c0' && ch <= '\u00ff';
    }

    public static boolean isExtendedLatin(char ch) {
        return ch >= '\u0100' && ch <= '\u017f';
    }

    public static boolean isArabic(char ch) {
        return ch >= '\u0600' && ch <= '\u06ff' ||
                ch >= '\u0750' && ch <= '\u077f' ||
                ch >= '\u08a0' && ch <= '\u08ff'
                ;
    }


    public abstract String filterToSpace(String s);

}
