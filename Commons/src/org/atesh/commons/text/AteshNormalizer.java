package org.atesh.commons.text;

public class AteshNormalizer {
    //static String TOSPACE=RankerNormalizer.SPACES+RankerNormalizer.PUNCTUATIONS;
    public static final char Fatheh = '\u064e';   //Arabic Fatheh
    public static final char Zameh = '\u064f';   //Arabic Zameh
    public static final char Kasreh = '\u0650';   //Arabic Kasreh
    public static final char Hamza = '\u0621';   //Arabic Hamza
    public static final char Tashdid = '\u0651';//ّ  Arabic Tashdid
    public static final char Tanvin_An = '\u064b';//ّ  Arabic an
    public static final char Tanvin_En = '\u064d';//ّ  Arabic en
    public static final char Tanvin_On = '\u064c';//ّ  Arabic on
    public static final char Ar_Tatweel = '\u0640'; // Arabic Keshide
    public static final char Fa_Ye = '\u0654'; // Farsi ye
    public static final String PUNCTUATIONS = "~!@#$%^&*()-=+,.<>/?\\;:'|\"[]{}«»" + "؟؛÷٪×،*٬" + "،؛,][\\}{+()«»:<>؟|";
    static final String TOREMOVE = "" + Ar_Tatweel + Tashdid + Hamza + Fa_Ye + Tanvin_An + Tanvin_En + Tanvin_On + Kasreh + Zameh + Fatheh;

//    public static final char Ar_Comma = '\u060c'; // Arabic Comma
//    public static final char Ar_SemiColon = '\u061b'; // Arabic Semi Colon
//    public static final char Ar_QuestionMark = '\u061f'; // Arabic Question mark
//    public static final char Ar_PercentSign = '\u066a'; // Arabic Percent Sign
//    private static final char FullStop = '\u002e';
//    private static final char LeftOepningDoubleAngle = '\u00ab';
//    private static final char RightOepningDoubleAngle = '\u00bb';
//    private static final char LeftArrow = '«';
//    private static final char RightArrow = '»';
//    private static final char Comma = ',';
//    private static final char SemiColon = ';';
//    private static final char Colon = ':';
//    private static final char ExclamationMark = '!';
//    private static final char QuestionMark = '?';
//    private static final char LeftOpeningparenthesis = '(';
//    private static final char RightOepningparenthesis = ')';
//    private static final char LeftCurlyBracket = '{';
//    private static final char RightCurlyBracket = '}';
//    private static final char LeftBracket = '[';
//    private static final char RightBracket = ']';
//    private static final char Sharp = '#';
//    private static final char AtSign = '@';
//    private static final char Slash = '/';
//    private static final char backslash = '\\';

    //    public static final String PUNCTUATIONS =  "" + Ar_Comma + Ar_PercentSign + Ar_QuestionMark + Ar_SemiColon + FullStop
//            + LeftOepningDoubleAngle + RightOepningDoubleAngle + Comma + SemiColon + Colon + ExclamationMark
//            + QuestionMark + LeftOpeningparenthesis + RightOepningparenthesis + LeftCurlyBracket + RightCurlyBracket +
//            AtSign + LeftArrow + RightArrow + backslash + Slash+LeftBracket+RightBracket+Sharp;
    static final char[] table = new char[256 * 256];
    private final static char[][] char2replace = {
            //
            {'۰', '0'},//
            {'۱', '1'},//
            {'۲', '2'},//
            {'۳', '3'},//
            {'۴', '4'},//
            {'۵', '5'},//
            {'۶', '6'},//
            {'۷', '7'},//
            {'۸', '8'},//
            {'۹', '9'},//
            {'٠', '0'},//
            {'١', '1'},//
            {'٢', '2'},//
            {'٣', '3'},//
            {'٤', '4'},//
            {'٥', '5'},//
            {'٦', '6'},//
            {'٧', '7'},//
            {'٨', '8'},//
            {'٩', '9'},//
            {'آ', 'ا'},//
            {'أ', 'ا'},//
            {'ؤ', 'و'},//
            {'إ', 'ا'},//
            {'ئ', 'ی'},//
            {'ي', 'ی'},//
            {'ئ', 'ی'},//
            {'ئ', 'ی'},//
            {'ئ', 'ی'},
            {'ة', 'ه'},
            {'ۀ', 'ه'},
            {'ء', 'ی'},
            {'ئ', 'ی'},//
            {'ا', 'ا'},//
            {'ب', 'ب'},//
            {'ة', 'ه'},//
            {'ت', 'ت'},//
            {'ث', 'ث'},//
            {'ج', 'ج'},//
            {'ح', 'ح'},//
            {'خ', 'خ'},//
            {'د', 'د'},//
            {'ذ', 'ذ'},//
            {'ر', 'ر'},//
            {'ز', 'ز'},//
            {'س', 'س'},//
            {'ش', 'ش'},//
            {'ص', 'ص'},//
            {'ض', 'ض'},//
            {'ط', 'ط'},//
            {'ظ', 'ظ'},//
            {'ع', 'ع'},//
            {'غ', 'غ'},//
            {'ف', 'ف'},//
            {'ق', 'ق'},//
            {'ك', 'ک'},//
            {'ل', 'ل'},//
            {'م', 'م'},//
            {'ن', 'ن'},//
            {'ه', 'ه'},//
            {'و', 'و'},//
            {'ى', 'ی'},//
            {'ي', 'ی'},//
            {'پ', 'پ'},//
            {'چ', 'چ'},//

            {'ٮ', 'ب'},//
            {'ٯ', 'ق'},//
            {'\u0671', 'ا'},//
            {'\u0672', 'ا'},//
            {'\u0673', 'ا'},//
            {'\u0675', 'ا'},//
            {'\u0676', 'و'},//
            {'\u0677', 'و'},//
            {'ٶ', 'و'},//
            {'ٷ', 'و'},//
            {'ٸ', 'ی'},//
            {'ٹ', 'ث'},//
            {'ٺ', 'ث'},//
            {'ٻ', 'ب'},//
            {'ټ', 'ت'},//
            {'ٽ', 'ت'},//
            {'ٿ', 'ت'},//
            {'ڀ', 'ب'},//
            {'ځ', 'ح'},//
            {'ڂ', 'خ'},//
            {'ڃ', 'ج'},//
            {'ڄ', 'ج'},//
            {'څ', 'خ'},//
            {'ڇ', 'چ'},//
            {'ڈ', 'ذ'},//
            {'ډ', 'د'},//
            {'ڊ', 'د'},//
            {'ڋ', 'ذ'},//
            {'ڌ', 'ذ'},//
            {'ڍ', 'د'},//
            {'ڎ', 'ڎ'},//
            {'ڏ', 'ذ'},//
            {'ڐ', 'ذ'},//
            {'ڑ', 'ر'},//
            {'ڒ', 'ر'},//
            {'ړ', 'ر'},//
            {'ڔ', 'ر'},//
            {'ڕ', 'ر'},//
            {'ږ', 'ر'},//
            {'ڗ', 'ز'},//
            {'ژ', 'ژ'},//
            {'ڙ', 'ژ'},//
            {'ښ', 'س'},//
            {'ڛ', 'س'},//
            {'ڜ', 'ش'},//
            {'ڝ', 'ص'},//
            {'ڞ', 'ض'},//
            {'ڟ', 'ظ'},//
            {'ڠ', 'غ'},//
            {'ڡ', 'ف'},//
            {'ڢ', 'ف'},//
            {'ڣ', 'ف'},//
            {'ڤ', 'ق'},//
            {'ڥ', 'ف'},//
            {'ڦ', 'ف'},//
            {'ڧ', 'ق'},//
            {'ڨ', 'ق'},//
            {'ک', 'ک'},//
            {'ڪ', 'ک'},//
            {'ګ', 'ک'},//
            {'ڬ', 'ک'},//
            {'ڭ', 'ک'},//
            {'ڮ', 'ک'},//
            {'گ', 'گ'},//
            {'ڰ', 'گ'},//
            {'ڱ', 'گ'},//
            {'ڲ', 'گ'},//
            {'ڳ', 'گ'},//
            {'ڴ', 'گ'},//
            {'ڵ', 'ل'},//
            {'ڶ', 'ل'},//
            {'ڷ', 'ل'},//
            {'ڸ', 'ل'},//
            {'ڹ', 'ن'},//
            {'ں', 'ن'},//
            {'ڻ', 'ن'},//
            {'ڼ', 'ن'},//
            {'ڽ', 'ن'},//
            {'ھ', 'ه'},//
            {'ڿ', 'چ'},//
            {'ۀ', 'ه'},//
            {'ہ', 'ه'},//
            {'ۂ', 'ه'},//
            {'ۃ', 'ه'},//
            {'ۄ', 'و'},//
            {'ۅ', 'و'},//
            {'ۆ', 'و'},//
            {'ۇ', 'و'},//
            {'ۈ', 'و'},//
            {'ۉ', 'و'},//
            {'ۊ', 'و'},//
            {'ۋ', 'و'},//
            {'ی', 'ی'},//
            {'ۍ', 'ی'},//
            {'ێ', 'ی'},//
            {'ۏ', 'و'},//
            {'ې', 'ی'},//
            {'ۑ', 'ی'},//
            {'ے', 'ی'},//
            {'ۓ', 'ی'},//
            {'ە', 'ه'},//
            {'ۮ', 'د'},//
            {'ۯ', 'ر'},//
            // Braille Patterns
            // Range: 2800— 28FF
            // Number of characters: 256
    };

    static {
        for (int i = 0; i < table.length; i++)
            table[i] = 0;
        for (int i = 0; i < char2replace.length; i++)
            table[char2replace[i][0]] = char2replace[i][1];

    }

    public static String removeSurrogate(String s) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < s.length(); i++)
            if (s.charAt(i) < Character.MIN_SURROGATE)
                builder.append(s.charAt(i));
        return builder.toString();
    }

    public static String normalize(String str) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < str.length(); i++) {
            char ch = str.charAt(i);
            if (TOREMOVE.indexOf(ch) >= 0)
                continue;

            if (ch >= Character.MIN_SURROGATE)
                ch = ' ';

            if (ch <= '\u001F') //Control Char
                ch = ' ';


            if (PUNCTUATIONS.indexOf(ch) >= 0)
                ch = ' ';

            if (ch < table.length && table[ch] != 0)
                ch = table[ch];

            builder.append(ch);
        }

        return StringUtils.white2Space(builder.toString());
    }


}