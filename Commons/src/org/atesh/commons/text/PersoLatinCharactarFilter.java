package org.atesh.commons.text;

/**
 * Created by saeed on 1/8/15.
 */
public class PersoLatinCharactarFilter extends ICharactarFilter {
    @Override
    public String filterToSpace(String s) {
        StringBuilder builder = new StringBuilder(s.length());
        for (int i = 0; i < s.length(); i++) {
            char ch = s.charAt(i);
            if (isArabic(ch) || isBasicLatin(ch) || isExtendedLatin(ch) || isSupplementLatin(ch))
                builder.append(ch);
            else
                builder.append(' ');
        }
        return builder.toString();
    }
}
