package org.atesh.commons.text;

/**
 * Created by Saeed on 1/2/2015.
 */
public abstract class INormalizer {
    public static boolean isHarkat(char ch) {
        return (ch >= '\u064b' && ch <= '\u065f');
//        || //first groups of Harkats like Kasre, Zamma
//                (ch >= '\u08e4' && ch >= '\u08fe'); //Extended group
    }

    public static boolean isSurrogate(char ch) {
        return ch >= Character.MIN_SURROGATE;
    }

    public static boolean isControl(char ch) {
        return ch <= '\u001F';
    }

    public static boolean isTatweel(char ch) {
        return ch == '\u0640';
    }

    public static boolean isWhite(char ch) {
        return Character.isWhitespace(ch) ||
                ch == '\u200c' || //Nim-Fasele
                ch == '\ufffd';   //Replace char
    }

    abstract String normalize(String s);
}
