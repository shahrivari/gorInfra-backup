package org.atesh.commons.text;

/**
 * Created by Saeed on 1/2/2015.
 */
public class CharacterMap {

    public static class ValidPersianLetters {

        //Alefs
        final static Character ArabicAlef = '\u0627';
        final static Character ArabicAlefBaKolah = '\u0622';
        final static Character ArabicAlefHamzaAbove = '\u0623';
        final static Character ArabicAlefHamzaBelow = '\u0625';

        final static Character ArabicBeh = '\u0628';
        final static Character ArabicPeh = '\u067e';
        final static Character ArabicTeh = '\u062a';
        final static Character ArabicTheh = '\u062b';

        final static Character ArabicJim = '\u062c';
        final static Character ArabicCheh = '\u0686';
        final static Character ArabicHah = '\u067d';
        final static Character ArabicKheh = '\u062e';

        final static Character ArabicDal = '\u062f';
        final static Character ArabicZal = '\u0630';
        final static Character ArabicReh = '\u0631';
        final static Character ArabicZeh = '\u0632';
        final static Character ArabicJeh = '\u0698';

        final static Character ArabicSin = '\u0633';
        final static Character ArabicShin = '\u0634';
        final static Character ArabicSad = '\u0635';
        final static Character ArabicZad = '\u0636';
        final static Character ArabicTah = '\u0637';
        final static Character ArabicZah = '\u0638';


        final static Character ArabicEyin = '\u0639';
        final static Character ArabicGhain = '\u063a';
        final static Character ArabicFeh = '\u0641';
        final static Character ArabicGhaf = '\u0642';

        final static Character FarsiKeh = '\u06a9';
        final static Character ArabicKaf = '\u0643'; //safe to normal to Keh
        final static Character ArabicGaf = '\u06af';

        final static Character ArabicLam = '\u0644';
        final static Character ArabicMim = '\u0645';
        final static Character ArabicNoon = '\u0646';

        //Waws
        final static Character ArabicWaw = '\u0648';
        final static Character ArabicWawHamzaAbove = '\u0624';

        //Hehs
        final static Character ArabicHeh = '\u0647';
        final static Character ArabicHehDotsAbove = '\u0620'; // Comes in Arabic mostly finally,  safe to normal to heh

        //Yehs
        final static Character FarsiYeh = '\u06cc';
        final static Character ArabicYeh = '\u064a';  // Shift+x in word safe to normal to Yeh
        final static Character ArabicYehHamzeBala = '\u0626'; // unsafe to noraml like: Ma'ede, Mas'ale, Esraeil. ....
        final static Character ArabicAlefMaqsura = '\u0649';  // safe to normal to Yeh like Kobra, Soghra,...
    }



    public static class InvalidPersianLetters{ //Should be removed
        final static Character ArabicAe = '\u06d5'; // A round Heh which comes only finally very strange!!! Goes to KURDU:::
        final static Character ArabicYehVeAbove = '\u06ce'; // strange Yeh !!! Goes to KURDU:::
    }





}
