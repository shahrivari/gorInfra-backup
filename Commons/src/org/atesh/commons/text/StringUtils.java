package org.atesh.commons.text;

/**
 * Created by saeed on 10/16/14.
 */
public class StringUtils {
    public static String white2Space(String s) {
        return s.replaceAll("\\s+", " ");
    }

    public static boolean hasSurrogate(String str) {
        for (int i = 0; i < str.length(); i++)
            if (str.charAt(i) >= Character.MIN_SURROGATE)
                return true;
        return false;
    }

    public static boolean hasControl(String str) {
        for (int i = 0; i < str.length(); i++)
            if (str.charAt(i) <= '\u001F')
                return true;
        return false;
    }

    public static boolean hasNewLine(String str) {
        for (int i = 0; i < str.length(); i++)
            if (str.charAt(i) == '\n' || str.charAt(i) == '\r')
                return true;
        return false;
    }

    public static String clearSurrogate(String str) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < str.length(); i++)
            if (str.charAt(i) >= Character.MIN_SURROGATE)
                builder.append(' ');
            else
                builder.append(str.charAt(i));

        return builder.toString();
    }


    public static String clearUTF(String str) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < str.length(); i++)
            if (str.charAt(i) <= '\u001F' || str.charAt(i) >= Character.MIN_SURROGATE)
                builder.append(' ');
            else
                builder.append(str.charAt(i));

        return builder.toString().replace("\\s+", " ");
    }


    public static String EscapeXML(String s) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < s.length(); i++) {
            char ch = s.charAt(i);
            switch (ch) {
                case '"':
                    builder.append("&quot;");
                    break;
                case '\'':
                    builder.append("&apos;");
                    break;
                case '<':
                    builder.append("&lt;");
                    break;
                case '>':
                    builder.append("&gt;");
                    break;
                case '&':
                    builder.append("&amp;");
                    break;
                default:
                    builder.append(ch);
            }
        }
        return builder.toString();
    }

}
