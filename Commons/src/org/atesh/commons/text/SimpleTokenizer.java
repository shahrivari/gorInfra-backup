package org.atesh.commons.text;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by saeed on 1/4/15.
 */
public class SimpleTokenizer extends ITokenizer {
    //protected static final String ValidChars="@_";
    private int maxLength = 20;

    @Override
    public List<String> tokenize(String s) {
        List<String> tokens = new ArrayList<String>(s.length() / 5);
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < s.length(); i++) {
            char ch = s.charAt(i);
            if (Character.isLetterOrDigit(ch)) {
                builder.append(ch);
                if (builder.length() >= maxLength) {
                    tokens.add(builder.toString());
                    builder.setLength(0);
                }
            } else if (builder.length() > 0) {
                tokens.add(builder.toString());
                builder.setLength(0);
            }
        }

        if (builder.length() > 0)
            tokens.add(builder.toString());

        return tokens;
    }
}
