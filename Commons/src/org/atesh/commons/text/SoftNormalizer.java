package org.atesh.commons.text;

/**
 * Created by Saeed on 1/2/2015.
 */
public class SoftNormalizer extends INormalizer {
    private static final char[] table = new char[256 * 256];

    private static void setCharMapping(char src, char dest) {
        table[src] = dest;
    }

    static {
        for (int i = 0; i < table.length; i++)
            table[i] = (char) 0;

        setCharMapping('۰', '0');//
        setCharMapping('۱', '1');//
        setCharMapping('۲', '2');//
        setCharMapping('۳', '3');//
        setCharMapping('۴', '4');//
        setCharMapping('۵', '5');//
        setCharMapping('۶', '6');//
        setCharMapping('۷', '7');//
        setCharMapping('۸', '8');//
        setCharMapping('۹', '9');//
        setCharMapping('٠', '0');//
        setCharMapping('١', '1');//
        setCharMapping('٢', '2');//
        setCharMapping('٣', '3');//
        setCharMapping('٤', '4');//
        setCharMapping('٥', '5');//
        setCharMapping('٦', '6');//
        setCharMapping('٧', '7');//
        setCharMapping('٨', '8');//
        setCharMapping('٩', '9');//
        setCharMapping('أ', 'ا');// Arabic Alef with Hamza Above
        setCharMapping('إ', 'ا');// Arabic Alef with Hamza Below
        setCharMapping('ؤ', 'و');// Arabic Waw with Hamza Above
        setCharMapping('ۇ', 'و');// Arabic U
        setCharMapping('ۈ', 'و');// Arabic Yu
        setCharMapping('\u08aa', 'ر');// Arabic Reh with Loop
        setCharMapping('ك', 'ک');// Arabic Kaf
        setCharMapping('\u06aa', 'ک');// Arabic Swash Kaf  (Keh jangulaki)
        setCharMapping('ي', 'ی');// Arabic Yeh
        setCharMapping('ى', 'ی');// Arabic Alef Maksura
        setCharMapping('\u0620', 'ی');// Kashmiri Yeh
        setCharMapping('ھ', 'ه'); // Heh Doachashmee
        setCharMapping('\u0629', 'ه'); // Arabic Tah Marbuta (Heh do noghteh)
        setCharMapping('ۀ', 'ه'); // Arabic Heh with Yeh Above
        setCharMapping('ە', 'ه'); // Arabic Ae
        setCharMapping('ء', ' '); // Arabic Hamza Seems unsafe
        setCharMapping('\u200c', ' '); // Nim-fasele
        SoftNormalizer.supportLegacy();
    }

    public static void supportLegacy() {
        setCharMapping('آ', 'ا'); // Alef-ba-kola
        setCharMapping('ئ', 'ی'); // Hamza yeh
    }

    public static void unsupportLegacy() {
        setCharMapping('آ', (char) 0); // Alef-ba-kola
        setCharMapping('ئ', (char) 0); // Hamza yeh
    }


    @Override
    public String normalize(String s) {
        String str = s.toLowerCase();
        StringBuilder builder = new StringBuilder(s.length());

        for (int i = 0; i < str.length(); i++) {
            char ch = str.charAt(i);
            if (isHarkat(ch) || isTatweel(ch)) //removes harkats and Tatweels
                continue;

            if (isSurrogate(ch)) //converts surrogates to space
                ch = ' ';

            if (isControl(ch)) //converts Control Chars to space
                ch = ' ';

            if (ch < table.length && table[ch] != (char) 0)
                ch = table[ch];

            builder.append(ch);
        }

        return builder.toString();
    }

}
