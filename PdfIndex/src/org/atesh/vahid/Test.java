package org.atesh.vahid;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

/**
 * Created by saeed on 3/4/15.
 */
public class Test {


    public static void main(String[] args) throws SQLException {

        String host = "pdf";
        String db = "saeed";
        String user = "root";
        String pass = "chamran";
        String databaseUrl = "jdbc:mysql://" + host + "/" + db + "?"
                + "useUnicode=true&useConfigs=maxPerformance&characterEncoding=UTF-8&user=" + user + "&password=" + pass;
        // create a connection source to our database
        ConnectionSource connectionSource =
                new JdbcConnectionSource(databaseUrl);

        // instantiate the dao
        Dao<PdfDoc, String> accountDao =
                DaoManager.createDao(connectionSource, PdfDoc.class);

        // if you need to create the 'accounts' table make this call
        TableUtils.createTable(connectionSource, PdfDoc.class);
        PdfDoc pdf = new PdfDoc();
        pdf.url = "http://xxx.com/";
        pdf.title = "saeed shahrivari";
        accountDao.create(pdf);
        connectionSource.close();

    }
}
