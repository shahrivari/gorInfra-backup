package org.atesh.vahid;

import com.google.common.base.Stopwatch;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import org.apache.spark.Accumulator;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by saeed on 2/2/15.
 */
public class PdfProcessor {
    public static void main(String[] args) throws SQLException {
        String appName = "PDF Processor";
        SparkConf conf = new SparkConf().setAppName(appName);
        conf.set("spark.executor.memory", "8g");
        conf.set("spark.serializer", "org.apache.spark.serializer.KryoSerializer");
        conf.set("spark.rdd.compress", "true");
        conf.set("spark.hadoop.mapred.output.compress", "true");
        conf.set("spark.hadoop.mapred.output.compression.codec", "true");
        conf.set("spark.hadoop.mapred.output.compression.codec", "org.apache.hadoop.io.compress.GzipCodec");
        conf.set("spark.hadoop.mapred.output.compression.type", "BLOCK");
        conf.set("spark.hadoop.dfs.replication", "2");

        JavaSparkContext sc = new JavaSparkContext(conf);
        Stopwatch stopwatch = Stopwatch.createStarted();
        Accumulator<Integer> docCount = sc.accumulator(0);
        Accumulator<Integer> expCount = sc.accumulator(0);

        String host = "pdf";
        String db = "saeed";
        String user = "root";
        String pass = "chamran";
        String databaseUrl = "jdbc:mysql://" + host + "/" + db + "?"
                + "useUnicode=true&useConfigs=maxPerformance&characterEncoding=UTF-8&user=" + user + "&password=" + pass;
        // create a connection source to our database
        ConnectionSource connectionSource1 =
                new JdbcConnectionSource(databaseUrl);

        // instantiate the dao
        Dao<PdfDoc, String> pdfDao1 =
                DaoManager.createDao(connectionSource1, PdfDoc.class);

        // if you need to create the 'accounts' table make this call
//        TableUtils.createTable(connectionSource1, PdfDoc.class);
//        connectionSource1.close();




        JavaRDD<String> jsons = sc.objectFile("hdfs://i7-01:9000/jsonpdf/");
        JavaRDD<String> parts = jsons.mapPartitions(iter -> {

            ConnectionSource connectionSource =
                    new JdbcConnectionSource(databaseUrl);

            // instantiate the dao
            Dao<PdfDoc, String> pdfDao =
                    DaoManager.createDao(connectionSource, PdfDoc.class);

            List<String> list = new ArrayList<String>();
            list.add("a");
            while (iter.hasNext()) {
                String json = iter.next();
                Gson gson = new GsonBuilder().create();
                Map<String, Object> map = gson.fromJson(json, Map.class);
                map.isEmpty();
                PdfDoc pdf = null;
                try {
                    pdf = PdfDoc.fromMap(map);
                    pdf.clearText();
                    pdfDao.create(pdf);
                } catch (ParseException e) {
                    expCount.add(1);
                }
                docCount.add(1);
            }
            connectionSource.close();
            return list;
        });


        System.out.printf("Parts: %,d \n", parts.count());
        System.out.printf("Good PDFs: %,d \n", docCount.value());
        System.out.printf("Exceptions GSON: %,d \n", expCount.value());
        System.out.println("Time: " + stopwatch);

        return;

    }

}
