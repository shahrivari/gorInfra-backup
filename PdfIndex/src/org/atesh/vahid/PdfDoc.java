package org.atesh.vahid;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import org.atesh.commons.text.StringUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

/**
 * Created by saeed on 3/2/15.
 */
@DatabaseTable(tableName = "pdfs")
public class PdfDoc {
    @DatabaseField(dataType = DataType.LONG_STRING)
    public String url = "";
    @DatabaseField(dataType = DataType.LONG_STRING)
    public String host = "";
    @DatabaseField(dataType = DataType.LONG_STRING)
    public String content = "";
    @DatabaseField
    public int numPages = 0;
    @DatabaseField(dataType = DataType.LONG_STRING)
    public String title = "";
    @DatabaseField(dataType = DataType.LONG_STRING)
    public String creator = "";
    @DatabaseField(dataType = DataType.LONG_STRING)
    public String author = "";
    @DatabaseField
    public Date modifyDate = new Date();
    @DatabaseField
    public Date createDate = new Date();
    @DatabaseField
    public String charset = "";
    @DatabaseField
    public String charsetConfidence = "";
    @DatabaseField(dataType = DataType.BYTE_ARRAY)
    public byte[] rawData = new byte[0];
    @DatabaseField(generatedId = true)
    private int id;

    public static PdfDoc fromMap(Map<String, Object> map) throws ParseException {
        PdfDoc doc = new PdfDoc();
        DateFormat df = new SimpleDateFormat("MMM dd, yyyy HH:mm:ss a");
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            if (entry.getKey().equals("BASE_URL"))
                doc.url = (String) entry.getValue();
            if (entry.getKey().equals("HOSTNAME"))
                doc.host = (String) entry.getValue();
            if (entry.getKey().equals("CONTENT_STRING"))
                doc.content = (String) entry.getValue();
            if (entry.getKey().equals("NUM_PDF_PAGES"))
                doc.numPages = ((Double) entry.getValue()).intValue();
            if (entry.getKey().equals("PDF_TITLE"))
                doc.title = (String) entry.getValue();
            if (entry.getKey().equals("PDF_CREATOR"))
                doc.creator = (String) entry.getValue();
            if (entry.getKey().equals("PDF_MODIFIED_DATE"))
                doc.modifyDate = df.parse((String) entry.getValue());
            if (entry.getKey().equals("PDF_CREATION_DATE"))
                doc.createDate = df.parse((String) entry.getValue());
            if (entry.getKey().equals("PDF_AUTHOR"))
                doc.author = (String) entry.getValue();
            if (entry.getKey().equals("PDF_CONTENT_CHARSET_ENCODING"))
                doc.charset = (String) entry.getValue();
            if (entry.getKey().equals("PDF_CONTENT_CHARSET_ENCODING_CONFIDENCE"))
                doc.charsetConfidence = (String) entry.getValue();
            if (entry.getKey().equals("RAW_CONTENT_BYTES")) {
                ArrayList<Double> list = (ArrayList<Double>) entry.getValue();
                doc.rawData = new byte[list.size()];
                for (int i = 0; i < list.size(); i++)
                    doc.rawData[i] = list.get(i).byteValue();
            }
        }
        return doc;
    }

    public void clearText() {
        url = StringUtils.clearUTF(url);
        host = StringUtils.clearUTF(host);
        content = StringUtils.clearUTF(content);
        title = StringUtils.clearUTF(title);
        creator = StringUtils.clearUTF(creator);
        author = StringUtils.clearUTF(author);
    }

//    public String toSql(String){
//        StringBuilder builder=new StringBuilder();
//
//
//    }

}
